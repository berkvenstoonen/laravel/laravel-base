<?php
declare(strict_types=1);

Route::post('/autocomplete/empty', fn () => response()->json(['results' => []]))->name('autocomplete.empty');
