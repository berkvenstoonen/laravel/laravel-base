@if(\App\Models\User::hasCurrent())
    <livewire:beto::user-toggle-dark-mode :user="\App\Models\User::getCurrent()"/>
@else
    <label class="dark-mode-toggle">
        <input type="checkbox" x-data="toggleDarkMode()">
        <!--suppress HtmlUnknownTag div is not allowed in label -->
        <div class="planet"></div>
        <!--suppress HtmlUnknownTag div is not allowed in label -->
        <div class="elements">
            <svg viewBox="0 0 500 500" xmlns="http://www.w3.org/2000/svg">
                <circle cx="250" cy="250" r="200"/>
            </svg>
            <svg viewBox="0 0 500 500" xmlns="http://www.w3.org/2000/svg">
                <circle cx="250" cy="250" r="200"/>
            </svg>
            <svg viewBox="0 0 500 500" xmlns="http://www.w3.org/2000/svg">
                <circle cx="250" cy="250" r="200"/>
            </svg>
            <svg viewBox="0 0 500 500" xmlns="http://www.w3.org/2000/svg">
                <circle cx="250" cy="250" r="200"/>
            </svg>
            <svg viewBox="0 0 500 500" xmlns="http://www.w3.org/2000/svg">
                <circle cx="250" cy="250" r="200"/>
            </svg>
            <svg viewBox="0 0 500 500" xmlns="http://www.w3.org/2000/svg">
                <circle cx="250" cy="250" r="200"/>
            </svg>
            <svg viewBox="0 0 500 500" xmlns="http://www.w3.org/2000/svg">
                <circle cx="250" cy="250" r="200"/>
            </svg>
            <svg viewBox="0 0 500 500" xmlns="http://www.w3.org/2000/svg">
                <circle cx="250" cy="250" r="200"/>
            </svg>
        </div>
    </label>
@endif
