@php use App\Models\User; @endphp
        <!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="{{ User::getCurrent()->getDarkModeClass() }}">
@if(isset($head))
    {{ $head }}
@else
    <x-beto::head/>
@endif
<body class="font-sans antialiased text-black">
<x-banner/>

<div class="min-h-screen bg-gray-100">
    <x-beto::navigation-menu/>
    @if (isset($header))
        <header class="bg-white shadow">
            <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
                {{ $header }}
            </div>
        </header>
    @endif
    <main>
        {{ $slot }}
    </main>
</div>

@stack('modals')
@livewireScriptConfig
@stack('scripts')
<script>
    window.sentryDns = "{{ config('sentry.dsn') }}";
    window.sentryUser = {!! json_encode(User::tryGetCurrent()) !!};
</script>
</body>
</html>
