<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@if(isset($head))
    {{ $head }}
@else
    <x-beto::head/>
@endif
<body class="font-sans antialiased text-black">
<div class="min-h-screen bg-gray-100">
    <x-beto::navigation-menu/>
    @if(config('auth.google'))
        <div id="g_id_onload"
             data-client_id="{{ config('auth.google.client_id') }}"
             data-context="signup"
             data-ux_mode="popup"
             data-login_uri="{{ route('login.google') }}"
             data-_token="{{csrf_token()}}"
             data-auto_select="true"
             data-itp_support="true">
        </div>
    @endif
    <main>
        {{ $slot }}
    </main>
</div>
@livewireScriptConfig
<script>
    window.sentryDns = "{{ config('sentry.dsn') }}";
    window.sentryUser = null;
</script>
</body>
</html>
