<button class="reset copy-to-clip margin-left-sm js-copy-to-clip js-tooltip-trigger" type="button" tabindex="0" onclick="navigator.clipboard.writeText('{{ $copyText }}').then(() => this.classList.add('copy-to-clip--copied'))">
    <svg class="icon icon--xs" width="16" height="16" viewBox="0 0 16 16">
        <title>@lang('beto::common.actions.copy')</title>
        <path d="M12,2h.5A1.5,1.5,0,0,1,14,3.5v10A1.5,1.5,0,0,1,12.5,15h-9A1.5,1.5,0,0,1,2,13.5V3.5A1.5,1.5,0,0,1,3.5,2H4" fill="none" stroke="currentColor" stroke-miterlimit="10" stroke-width="2"></path>
        <rect x="6" y="1" width="4" height="2" fill="currentColor" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"></rect>
        <polyline class="copy-to-clip__icon-check" points="5 9 7 11 11 7" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"></polyline>
    </svg>
</button>
