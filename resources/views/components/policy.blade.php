<div class="pt-4 bg-gray-100">
    <div class="min-h-screen flex flex-col items-center pt-6 sm:pt-0 px-6">
        <div>
            <x-authentication-card-logo/>
        </div>
        <div class="page-block prose mt-8">
            <h1>Privacy Policy</h1>
            <p class="font-bold">Last updated 2023-01-05</p>
            <p>This privacy notice for Stichting BeTo ("<span class="font-bold">Company</span>," "<span class="font-bold">we</span>," "<span class="font-bold">us</span>," or "<span class="font-bold">our</span>"), describes how and why we might collect, store, use, and/or share ("<span class="font-bold">process</span>") your information when you use our services ("<span class="font-bold">Services</span>"), such as when you:</p>
            <ul>
                <li>Visit website at <a href="{{ config('app.url') }}">{{ config('app.url') }}</a>, or any website of ours that links to this privacy notice</li>
                <li>Engage with us in other related ways, including any sales, marketing, or events</li>
            </ul>
            <p><span class="font-bold">Questions or concerns?</span> Reading this privacy notice will help you understand your privacy rights and choices. If you do not agree with our policies and practices, please do not use our Services. If you still have any questions or concerns, please contact us at <a href="mailto:info@be-to.nl">info@be-to.nl</a>.</p>
        </div>
        <div class="page-block prose">
            <h2>Summary of key points</h2>
            <i>This summary provides key points from our privacy notice, but you can find out more details about any of these topics by clicking the link following each key point or by using our table of contents below to find the section you are looking for. You can also click <a href="#toc">here</a> to go directly to our table of contents.</i>
            <p><span class="font-bold">What personal information do we process?</span> When you visit, use, or navigate our Services, we may process personal information depending on how you interact with us and the Services, the choices you make, and the products and features you use. Click <a href="#personalinfo">here</a> to learn more.</p>
            <p><span class="font-bold">Do we process any sensitive personal information?</span> We do not process sensitive personal information.</p>
            <p><span class="font-bold">Do we receive any information from third parties?</span> We do not receive any information from third parties.</p>
            <p><span class="font-bold">How do we process your information?</span> We process your information to provide, improve, and administer our Services, communicate with you, for security and fraud prevention, and to comply with law. We may also process your information for other purposes with your consent. We process your information only when we have a valid legal reason to do so. Click <a href="#infouse">here</a> to learn more.</p>
            <p><span class="font-bold">In what situations and with which parties do we share personal information?</span> We may share information in specific situations and with specific third parties. Click <a href="#whoshare">here</a> to learn more.</p>
            <p><span class="font-bold">How do we keep your information safe?</span> We have organizational and technical processes and procedures in place to protect your personal information. However, no electronic transmission over the internet or information storage technology can be guaranteed to be 100% secure, so we cannot promise or guarantee that hackers, cybercriminals, or other unauthorized third parties will not be able to defeat our security and improperly collect, access, steal, or modify your information. Click <a href="#infosafe">here</a> to learn more.</p>
            <p><span class="font-bold">What are your rights?</span> Depending on where you are located geographically, the applicable privacy law may mean you have certain rights regarding your personal information. Click <a href="#privacyrights">here</a> to learn more.</p>
            <p><span class="font-bold">How do you exercise your rights?</span> The easiest way to exercise your rights is by managing the information we have from you which can all be found here: <a href="{{ url(route('profile.show')) }}">profile</a>, or by contacting us. We will consider and act upon any request in accordance with applicable data protection laws.</p>
            <p>Want to learn more about what Stichting BeTo does with any information we collect? Click <a href="#toc">here</a> to review the notice in full.</p>
        </div>
        <div id="toc" class="page-block prose">
            <h2>Table Of Contents</h2>
            <ol>
                <li><a href="#infocollect">What information do we collect?</a></li>
                <li><a href="#infouse">How do we process your information?</a></li>
                <li><a href="#legalbases">What legal bases do we rely on to process your personal information?</a></li>
                <li><a href="#whoshare">When and with whom do we share your personal information?</a></li>
                <li><a href="#inforetain">How long do we keep your information?</a></li>
                <li><a href="#infosafe">How do we keep your information safe?</a></li>
                <li><a href="#infominors">Do we collect information from minors?</a></li>
                <li><a href="#privacyrights">What are your privacy rights?</a></li>
                <li><a href="#DNT">Controls for do-not-track features</a></li>
                <li><a href="#policyupdates">Do we make updates to this notice?</a></li>
                <li><a href="#contact">How can you contact us about this notice?</a></li>
                <li><a href="#request">How can you review, update, or delete the data we collect from you?</a></li>
            </ol>
        </div>
        <div id="infocollect" class="page-block prose">
            <h2>1. What information do we collect?</h2>
            <h3 id="personalinfo" class="text-gray-900">Personal information you disclose to us</h3>
            <i><span class="font-bold">In Short:</span> We collect personal information that you provide to us.</i>
            <p>We collect personal information that you voluntarily provide to us when you register on the Services, express an interest in obtaining information about us or our products and Services, when you participate in activities on the Services, or otherwise when you contact us.</p>
            <p><span class="font-bold">Personal Information Provided by You.</span> The personal information that we collect depends on the context of your interactions with us and the Services, the choices you make, and the products and features you use. The personal information we collect may include the following:</p>
            <ul>
                <li>Names</li>
                <li>Phone numbers</li>
                <li>Email addresses</li>
                <li>Passwords</li>
            </ul>
            <p><span class="font-bold">Sensitive Information.</span> We do not process sensitive information.</p>
            <p>All personal information that you provide to us must be true, complete, and accurate, and you must notify us of any changes to such personal information.</p>
        </div>
        <div id="infouse" class="page-block prose">
            <h2>2. How do we process your information?</h2>
            <i><span class="font-bold">In Short:</span> We process your information to provide, improve, and administer our Services, communicate with you, for security and fraud prevention, and to comply with law. We may also process your information for other purposes with your consent.</i>
            <p>We process your personal information for a variety of reasons, depending on how you interact with our Services, including:</p>
            <ul>
                <li><span class="font-bold">To facilitate account creation and authentication and otherwise manage user accounts.</span> We may process your information so you can create and log in to your account, as well as keep your account in working order.</li>
                <li><span class="font-bold">To save or protect an individual's vital interest.</span> We may process your information when necessary to save or protect an individual's vital interest, such as to prevent harm.</li>
            </ul>
        </div>
        <div id="legalbases" class="page-block prose">
            <h2>3. What legal bases do we rely on to process your information?</h2>
            <i><span class="font-bold">In Short:</span> We only process your personal information when we believe it is necessary and we have a valid legal reason (i.e., legal basis) to do so under applicable law, like with your consent, to comply with laws, to provide you with services to enter into or fulfill our contractual obligations, to protect your rights, or to fulfill our legitimate business interests.</i>
            <p>The General Data Protection Regulation (GDPR) and UK GDPR require us to explain the valid legal bases we rely on in order to process your personal information. As such, we may rely on the following legal bases to process your personal information:</p>
            <ul>
                <li><span class="font-bold">Consent.</span> We may process your information if you have given us permission (i.e., consent) to use your personal information for a specific purpose. You can withdraw your consent at any time. Click <a href="#withdrawconsent">here</a> to learn more.</li>
                <li><span class="font-bold">Legal Obligations.</span> We may process your information where we believe it is necessary for compliance with our legal obligations, such as to cooperate with a law enforcement body or regulatory agency, exercise or defend our legal rights, or disclose your information as evidence in litigation in which we are involved.</li>
                <li><span class="font-bold">Vital Interests.</span> We may process your information where we believe it is necessary to protect your vital interests or the vital interests of a third party, such as situations involving potential threats to the safety of any person.</li>
            </ul>
        </div>
        <div id="whoshare" class="page-block prose">
            <h2>4. When and with whom do we share your personal information?</h2>
            <i><span class="font-bold">In Short:</span> We may share information in specific situations described in this section and/or with the following third parties.</i>
            <p>The General Data Protection Regulation (GDPR) and UK GDPR require us to explain the valid legal bases we rely on in order to process your personal information. As such, we may rely on the following legal bases to process your personal information:</p>
            <ul>
                <li><span class="font-bold">Consent.</span> We may process your information if you have given us permission (i.e., consent) to use your personal information for a specific purpose. You can withdraw your consent at any time. Click <a href="#withdrawconsent">here</a> to learn more.</li>
                <li><span class="font-bold">Legal Obligations.</span> We may process your information where we believe it is necessary for compliance with our legal obligations, such as to cooperate with a law enforcement body or regulatory agency, exercise or defend our legal rights, or disclose your information as evidence in litigation in which we are involved.</li>
                <li><span class="font-bold">Vital Interests.</span> We may process your information where we believe it is necessary to protect your vital interests or the vital interests of a third party, such as situations involving potential threats to the safety of any person.</li>
            </ul>
            <p>Test</p>
            <ul>
                <li>
                    <span class="font-bold">Sentry.io</span> We potentially share your information with Sentry.io as we use Sentry.io to log errors that occur on our applications. These errors can contain information about the user (if you are signed in) that we need to try and reproduce and fix the error.
                    <ul>
                        <li>We refer you to their own <a href="https://sentry.io/security/#data-flow" target="_blank">Security & Compliance</a> page</li>
                        <li>Note that this link takes you to the Data Flow chapter as your information will only be ingested as data so other parts of this document are less relevant for you.</li>
                    </ul>
                </li>
            </ul>
        </div>
        <div id="inforetain" class="page-block prose">
            <h2>5. How long do we keep your information?</h2>
            <i><span class="font-bold">In Short:</span> We keep your information for as long as necessary to fulfill the purposes outlined in this privacy notice unless otherwise required by law.</i>
            <p>We will only keep your personal information for as long as it is necessary for the purposes set out in this privacy notice, unless a longer retention period is required or permitted by law (such as tax, accounting, or other legal requirements). No purpose in this notice will require us keeping your personal information for longer than the period of time in which users have an account with us.</p>
            <p>When we have no ongoing legitimate business need to process your personal information, we will either delete or anonymize such information, or, if this is not possible (for example, because your personal information has been stored in backup archives), then we will securely store your personal information and isolate it from any further processing until deletion is possible.</p>
        </div>
        <div id="infosafe" class="page-block prose">
            <h2>6. How do we keep your information safe?</h2>
            <i><span class="font-bold">In Short:</span> We aim to protect your personal information through a system of organizational and technical security measures.</i>
            <p>We have implemented appropriate and reasonable technical and organizational security measures designed to protect the security of any personal information we process. However, despite our safeguards and efforts to secure your information, no electronic transmission over the Internet or information storage technology can be guaranteed to be 100% secure, so we cannot promise or guarantee that hackers, cybercriminals, or other unauthorized third parties will not be able to defeat our security and improperly collect, access, steal, or modify your information. Although we will do our best to protect your personal information, transmission of personal information to and from our Services is at your own risk. You should only access the Services within a secure environment.</p>
            <p>Below we provide a list of measures that we currently deploy in order to keep your data as safe as possible:</p>
            <ul>
                <li><span class="font-bold">HTTPS:</span> We use HTTPS with certificates from <a href="https://letsencrypt.org/" target="_blank">LetsEncrypt</a> to provide encrypted communication between your client devices and our servers</li>
                <li><span class="font-bold">Self Owned Hardware:</span> We have all server hardware in house, this means that all data is stored on servers located in The Netherlands and fall completely under Dutch privacy and data protection laws.</li>
            </ul>
            <p>If you have any recommendations to our security measures, please contact us at <a href="mailto:security@be-to.nl">security@be-to.nl</a>.</p>
        </div>
        <div id="infominors" class="page-block prose">
            <h2>7. Do we collect information from minors?</h2>
            <i><span class="font-bold">In Short:</span> We do not knowingly collect data from or market to children under 18 years of age.</i>
            <p>We do not knowingly solicit data from or market to children under 18 years of age. By using the Services, you represent that you are at least 18 or that you are the parent or guardian of such a minor and consent to such minor dependent's use of the Services. If we learn that personal information from users less than 18 years of age has been collected, we will deactivate the account and take reasonable measures to promptly delete such data from our records. If you become aware of any data we may have collected from children under age 18, please contact us at <a href="mailto:privacy@be-to.nl">privacy@be-to.nl</a>.</p>
        </div>
        <div id="privacyrights" class="page-block prose">
            <h2>8. What are your privacy rights?</h2>
            <i><span class="font-bold">In Short:</span> In the European Economic Area (EEA), you have rights that allow you greater access to and control over your personal information. You may review, change, or terminate your account at any time.</i>
            <p>In the EEA, you have certain rights under applicable data protection laws. Including the right to request access and obtain a copy of your personal information, to request rectification or erasure; to restrict the processing of your personal information; and if applicable, to data portability. In certain circumstances, you may also have the right to object to the processing of your personal information. You can make such a request by contacting us by using the contact details provided in the section "<a href="#contact">How can you contact us about this notice?</a>" below.</p>
            <p>We will consider and act upon any request in accordance with applicable data protection laws.</p>
            <p>If you believe we are unlawfully processing your personal information, you also have the right to complain to your local data protection supervisory authority. You can find their contact details here: <a href="https://ec.europa.eu/justice/data-protection/bodies/authorities/index_en.htm" target="_blank">https://ec.europa.eu/justice/data-protection/bodies/authorities/index_en.htm</a>.</p>
            <p><span id="withdrawconsent" class="font-bold">Withdrawing your consent:</span> If we are relying on your consent to process your personal information, you have the right to withdraw your consent at any time. You can withdraw your consent at any time by contacting us by using the contact details provided in the section "<a href="#contact">How can you contact us about this notice?</a>" below.</p>
            <p>However, please note that this will not affect the lawfulness of the processing before its withdrawal nor, will it affect the processing of your personal information conducted in reliance on lawful processing grounds other than consent.</p>
            <h3 class="text-gray-900">Account Information</h3>
            <p>If you would at any time like to review or change the information in your account or terminate your account, you can:</p>
            <ul>
                <li>Log in to your account settings and update your user account.</li>
            </ul>
            <p>Upon your request to terminate your account, we will deactivate or delete your account and information from our active databases. However, we may retain some information in our files to prevent fraud, troubleshoot problems, assist with any investigations, enforce our legal terms and/or comply with applicable legal requirements.</p>
            <p>If you have questions or comments about your privacy rights, you may email us at <a href="mailto:privacy@be-to.nl">privacy@be-to.nl</a>.</p>
        </div>
        <div id="DNT" class="page-block prose">
            <h2>9. Controls for do-not-track features</h2>
            <p>Most web browsers and some mobile operating systems and mobile applications include a Do-Not-Track ("DNT") feature or setting you can activate to signal your privacy preference not to have data about your online browsing activities monitored and collected. At this stage no uniform technology standard for recognizing and implementing DNT signals has been finalized. As such, we do not currently respond to all different DNT browser signals or any other mechanism that automatically communicates your choice not to be tracked online. If a standard for online tracking is adopted that we must follow in the future, we will inform you about that practice in a revised version of this privacy notice.</p>
            <p>We do currently support the DNT header as defined here: <a href="https://www.w3.org/TR/tracking-dnt/#dnt-header-field" target="_blank">https://www.w3.org/TR/tracking-dnt/#dnt-header-field</a> with a fallback to HTTP_DNT (as used by Chrome). If this header is sent we will not send any information to Sentry.io and therefore might be unable to help with errors that occur (no errors should occur from using this header).</p>
            <p>If you would like us to support a different DNT implementation, please contact us at <a href="mailto:privacy@be-to.nl">privacy@be-to.nl</a>.</p>
        </div>
        <div id="policyupdates" class="page-block prose">
            <h2>10. Do we make updates to this notice?</h2>
            <i><span class="font-bold">In Short:</span> Yes, we will update this notice as necessary to stay compliant with relevant laws.</i>
            <p>We may update this privacy notice from time to time. The updated version will be indicated by an updated "Revised" date and the updated version will be effective as soon as it is accessible. If we make material changes to this privacy notice, we may notify you either by prominently posting a notice of such changes or by directly sending you a notification. We encourage you to review this privacy notice frequently to be informed of how we are protecting your information.</p>
        </div>
        <div id="contact" class="page-block prose">
            <h2>11. How can you contact us about this notice?</h2>
            <p>If you have questions or comments about this notice, you may email us at <a href="mailto:privacy@be-to.nl">privacy@be-to.nl</a> or by post to:</p>
            <p class="pl-6">
                <span>Stichting BeTo</span><br/>
                <span>Steenakker 4</span><br/>
                <span>Leende, Noord-Brabant 5595HP</span><br/>
                <span>Netherlands</span><br/>
            </p>
        </div>
        <div id="request" class="page-block prose">
            <h2>12. How can you review, update, or delete the data we collect from you?</h2>
            <p>You have the right to request access to the personal information we collect from you, change that information, or delete it. To request to review, update, or delete your personal information, please visit: <a href="{{ url(route('profile.show')) }}">profile</a>.</p>
        </div>
    </div>
</div>
