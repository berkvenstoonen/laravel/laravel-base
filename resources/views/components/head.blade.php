<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    @vite(['resources/css/app.scss', 'resources/js/app.js'])
    @if(config('auth.google'))
        <script src="https://accounts.google.com/gsi/client" async defer></script>
    @endif
    @if(config('beto.include.font-awesome'))
        <script src="https://kit.fontawesome.com/c93011a487.js" crossorigin="anonymous"></script>
    @endif

    <!-- Styles -->
    @livewireStyles

    <!-- Icons -->
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('site.webmanifest') }}">
    <link rel="mask-icon" href="{{ asset('safari-pinned-tab.svg') }}" color="{{ config('theme.color', '#be2dd8') }}">
    <meta name="apple-mobile-web-app-title" content="{{ config('app.name') }}">
    <meta name="application-name" content="{{ config('app.name') }}">
    <meta name="msapplication-TileColor" content="{{ config('theme.background-color', '#3f2dd8') }}">
    <meta name="theme-color" content="{{ config('theme.background-color', '#3f2dd8') }}">

    <meta name="image-error-url" content="{{ url('images/image-error.svg') }}">
    <meta name="dark-image-error-url" content="{{ url('images/image-error-white.svg') }}">
</head>
