<div class="pt-12">
    <div class="max-w-8xl mx-auto sm:px-6 lg:px-8">
        <div {{ $attributes->merge(['class' => 'bg-white shadow-xl sm:rounded-lg']) }}>
            {{ $slot }}
        </div>
    </div>
</div>
