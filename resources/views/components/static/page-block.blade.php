<x-beto::static.base-block>
    <div {{ $attributes->merge(['class' => 'px-4 py-5 bg-white sm:p-6']) }}>
        {{ $slot }}
    </div>
</x-beto::static.base-block>
