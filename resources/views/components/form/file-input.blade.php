<div
        wire:ignore
        x-data
        x-init="
            void(0);
            const pond = FilePond.create($refs.input);
            pond.setOptions({
                allowMultiple: {{ $attributes->has('multiple') ? 'true' : 'false' }},
                server: {
                    process: (fieldName, file, metadata, load, error, progress, abort, transfer, options) => {
                        @this.upload('{{ $attributes->get('wire:model') }}', file, load, error, (progressEvent) => {
                            progress(progressEvent.lengthComputable, progressEvent.loaded, progressEvent.total);
                        });
                    },
                    revert: (filename, load, error) => {
                        @this.removeUpload('{{ $attributes->get('wire:model') }}', filename, load);
                    }
                }
            });
            pond.on('addfile', (error, file) => {
                if (file.fileExtension === 'stl') {
                    window.dodrop(file.file);
                }
            });
            navigator.serviceWorker.addEventListener('message', event => {
                if (event.data.action === 'share-upload') {
                    pond.addFile(event.data.file);
                }
            });
            window.addEventListener('clearFile', () => pond.removeFiles())
        "
        @if($attributes->has('x-show'))
        x-show="{{ $attributes->get('x-show') }}"
        @endif
>
    <input type="file" {{ $attributes->except(['x-show', 'wire:model']) }} x-ref="input">
</div>
