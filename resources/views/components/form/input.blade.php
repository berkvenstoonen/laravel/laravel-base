<div class="col-span-12 {{ $fullWidth ? '' : 'sm:col-span-6' }} {{ $padding ? 'mt-4 mx-2' : '' }}" @if($attributes->has('x-show')) x-show="{{ $attributes->get('x-show') }}" @endif>
    @if (!isset($labelHtml))
        <x-label for="{{ $id }}">{{ $label }}</x-label>
    @else
        {{ $labelHtml }}
    @endif
    @if (!isset($inputHtml))
        <x-input id="{{ $id }}" type="{{ $type }}" {{ $attributes }} class="w-full"/>
    @else
        {{ $inputHtml }}
    @endif
    @if (!isset($errorHtml))
        <x-input-error for="{{ $id }}" class="mt-2"/>
    @else
        {{ $errorHtml }}
    @endif
</div>
