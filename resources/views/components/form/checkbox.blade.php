<div class="col-span-12 sm:col-span-6 mt-4 mx-2" @if($attributes->has('x-show')) x-show="{{ $attributes->get('x-show') }}" @endif>
    @if (!isset($labelHtml))
        <x-label class="inline mr-2" for="{{ $id }}">{{ $label }}</x-label>
    @else
        {{ $labelHtml }}
    @endif
    @if (!isset($inputHtml))
        <x-checkbox id="{{ $id }}" value="Yes" {{ $attributes }}/>
    @else
        {{ $inputHtml }}
    @endif
    @if (!isset($errorHtml))
        <x-input-error for="{{ $id }}" class="mt-2"/>
    @else
        {{ $errorHtml }}
    @endif
</div>
