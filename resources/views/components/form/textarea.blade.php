<div class="col-span-12 {{ $fullWidth ? '' : 'sm:col-span-6' }} {{ $padding ? 'mt-4 mx-2' : '' }}" @if($attributes->has('x-show')) x-show="{{ $attributes->get('x-show') }}" @endif>
    @if (!isset($labelHtml))
        <x-label for="{{ $id }}">{{ $label }}</x-label>
    @else
        {{ $labelHtml }}
    @endif
    @if (!isset($inputHtml))
        <textarea id="message" rows="3" class="block p-2.5 mt-1 w-full text-sm text-gray-900 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 w-full" {{ $attributes->except(['x-show', 'item']) }}></textarea>
    @else
        {{ $inputHtml }}
    @endif
    @if (!isset($errorHtml))
        <x-input-error for="{{ $id }}" class="mt-2"/>
    @else
        {{ $errorHtml }}
    @endif
</div>
