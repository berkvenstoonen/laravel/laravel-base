@props(['id' => null])
<x-beto::modal :id="$id" {{ $attributes->only('wire:model') }}>
    <form method="POST" {{ $attributes->except('wire:model') }} wire:submit.prevent="save">
        @method($method)
        @csrf
        <div class="modal-body px-6 py-4">
            <div class="text-lg">
                {{ $title }}
            </div>

            <div class="mt-4">
                <div class="grid">
                    {{ $content }}
                </div>
            </div>
        </div>

        <div class="px-6 py-4 bg-gray-100 text-right">
            @if ($cancelUrl !== null)
                <x-beto::secondary-button href="{{ url($cancelUrl) }}">@lang('beto::common.actions.cancel')</x-beto::secondary-button>
            @endif
            <x-button>@lang('beto::common.actions.save')</x-button>
        </div>
    </form>
</x-beto::modal>
