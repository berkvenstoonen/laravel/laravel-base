<div wire:ignore class="col-span-12 mt-4 mx-2" @if($attributes->has('x-show')) x-show="{{ $attributes->get('x-show') }}" @endif>
    @if(!isset($labelHtml))
        <x-label for="{{ $id }}">{{ $label }}</x-label>
    @else
        {{ $labelHtml }}
    @endif
    <!--suppress HtmlFormInputWithoutLabel -->
    <select
            id="{{ $id }}"
            @if($named)
                name="{{ $attributes->get('model') }}"
            @endif
            {{ $attributes->class(['form-control']) }}
            x-data="select2"
            data-wire-model="{{ $attributes->get('model') }}"
            style="width: 100%"
            data-allow-custom="{{ $allowCustom ? 1 : 0 }}"
            data-placeholder="{{ trans('beto::common.actions.select-option') }}"
            @if($autocompleteUrl !== null)
                data-empty-url="{{ route('autocomplete.empty') }}"
            data-autocomplete-url="{{ $autocompleteUrl }}"
            data-min-autocomplete-length="{{ $autocompleteLength }}"
            data-autocomplete-headers="{{ json_encode($autocompleteHeaders, JSON_FORCE_OBJECT | JSON_THROW_ON_ERROR) }}"
            data-autocomplete-filters="{{ json_encode($autocompleteFilters, JSON_FORCE_OBJECT | JSON_THROW_ON_ERROR) }}"
            @endif
            @if($multiple)
                multiple
            @endif
    >
        <option></option>
        @foreach($tags as $tag)
            <option value="{{ $tag['id'] }}" @if((is_array($selected) && in_array($tag['id'], $selected)) || $tag['id'] === $selected) selected @endif>{{ $tag['text'] }}</option>
        @endforeach
    </select>
</div>
