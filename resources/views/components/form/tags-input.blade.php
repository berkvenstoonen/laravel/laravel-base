<div x-data @tag-selection-updated="$wire.setValue('{{ $attributes->get('wire:model') }}', $event.detail.selected)" data-selected='{!! json_encode($selected) !!}' data-tags='{!! json_encode($tags) !!}' class="col-span-12 {{ $fullWidth ? '' : 'sm:col-span-6' }} mt-4 mx-2" @if($attributes->has('x-show')) x-show="{{ $attributes->get('x-show') }}" @endif>
    <div x-data="tagSelect()" x-init="init()" @click.away="clearSearch()" @click="search('')" @keydown.escape="clearSearch">
        <div class="relative" @keydown.enter.prevent="selectOrAddTag(textInput)" @keydown.tab="selectTag(textInput, event)" @keydown.arrow-down.prevent="focusNext()" @keydown.arrow-up.prevent="focusPrevious()">
            @if (!isset($labelHtml))
                <x-label for="{{ $id }}">{{ $label }}</x-label>
            @else
                {{ $labelHtml }}
            @endif
            @if (!isset($inputHtml))
                <x-input id="{{ $id }}" type="text" x-model="textInput" x-ref="textInput" @input="search($event.target.value)" {{ $attributes->except('wire:model') }}/>
            @else
                {{ $inputHtml }}
            @endif
            <div :class="[open ? 'block' : 'hidden']">
                <div class="absolute z-40 left-0 w-full">
                    <div class="py-1 text-sm bg-white rounded shadow-lg border border-gray-300">
                        <template x-for="(tag, index) in found">
                            <a @click.prevent="addTag(tag)" class="tag-completion block py-1 px-5 cursor-pointer hover:bg-indigo-600 hover:text-white" x-text="tag"></a>
                        </template>
                        <a @click.prevent="addTag(textInput)" :class="[showCreate ? 'block' : 'hidden']" class="tag-completion block py-1 px-5 cursor-pointer hover:bg-indigo-600 hover:text-white">Add tag "<span class="font-semibold" x-text="textInput"></span>"</a>
                    </div>
                </div>
            </div>
            <!-- selections -->
            <div class="tags-container" @click="$refs.textInput.focus()">
                <template x-for="(tag, index) in selected">
                    <x-badge>
                        <span class="ml-2 mr-1 leading-relaxed truncate max-w-xs" x-text="tag"></span>
                        <button type="button" @click.stop="removeTag(index)" class="w-6 h-8 inline-block align-middle text-gray-500 hover:text-gray-600">
                            <svg class="w-6 h-6 fill-current mx-auto" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                                <path fill-rule="evenodd" d="M15.78 14.36a1 1 0 0 1-1.42 1.42l-2.82-2.83-2.83 2.83a1 1 0 1 1-1.42-1.42l2.83-2.82L7.3 8.7a1 1 0 0 1 1.42-1.42l2.83 2.83 2.82-2.83a1 1 0 0 1 1.42 1.42l-2.83 2.83 2.83 2.82z"/>
                            </svg>
                        </button>
                    </x-badge>
                </template>
            </div>
            @if (!isset($errorHtml))
                <x-input-error for="{{ $id }}" class="mt-2"/>
            @else
                {{ $errorHtml }}
            @endif
        </div>
    </div>
</div>
