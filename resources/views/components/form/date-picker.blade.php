<div class="col-span-12 {{ $fullWidth ? '' : 'sm:col-span-6' }} {{ $padding ? 'mt-4 mx-2' : '' }}" @if($attributes->has('x-show')) x-show="{{ $attributes->get('x-show') }}" @endif>
    @if (!isset($labelHtml))
        <x-label for="{{ $id }}">{{ $label }}</x-label>
    @else
        {{ $labelHtml }}
    @endif
    @if (!isset($inputHtml))
        <div class="relative">
            <i class="flex p-2 h-5 fa-regular fa-calendar absolute"></i>
            <x-input x-data="datepicker" id="{{ $id }}" type="{{ $type }}" class="pl-9 w-full" {{  $attributes->merge(['placeholder' => trans('beto::common.actions.select-date'), 'datepicker-orientation' => 'bottom'])  }}/>
        </div>
    @else
        {{ $inputHtml }}
    @endif
    @if (!isset($errorHtml))
        <x-input-error for="{{ $id }}" class="mt-2"/>
    @else
        {{ $errorHtml }}
    @endif
</div>
