<div class="col-span-12 {{ $fullWidth ? '' : 'sm:col-span-6' }} mt-4 mx-2" @if($attributes->has('x-show')) x-show="{{ $attributes->get('x-show') }}" @endif>
    @if (!isset($labelHtml))
        <x-label for="{{ $id }}">{{ $label }}</x-label>
    @else
        {{ $labelHtml }}
    @endif
    @if (!isset($inputHtml))
        <!--suppress HtmlFormInputWithoutLabel -->
        <select id="{{ $id }}" {{ $attributes }} class="mt-1 w-full border border-gray-300 rounded-md shadow-sm pl-3 pr-10 py-2 text-left cursor-default focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
            @if($empty)
                <option disabled selected>@lang('beto::common.actions.select')</option>
            @endif
            @foreach($values as $value => $label)
                <option value="{{ $value }}">{{ $label }}</option>
            @endforeach
        </select>
    @else
        {{ $inputHtml }}
    @endif
    @if (!isset($errorHtml))
        <x-input-error for="{{ $id }}" class="mt-2"/>
    @else
        {{ $errorHtml }}
    @endif
</div>
