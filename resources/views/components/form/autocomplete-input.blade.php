<div>
    <div
            x-data="{
                open: @entangle('showAutocomplete'),
                autocomplete: @entangle('autocomplete'),
                selected: @entangle('selected'),
                highlightedIndex: 0,
                highlightPrevious() {
                    if (this.highlightedIndex > 0) {
                        this.highlightedIndex = this.highlightedIndex - 1;
                        this.scrollIntoView();
                    }
                },
                highlightNext() {
                    if (this.highlightedIndex < this.$refs.results.children.length - 1) {
                        this.highlightedIndex = this.highlightedIndex + 1;
                        this.scrollIntoView();
                    }
                },
                scrollIntoView() {
                    this.$refs.results.children[this.highlightedIndex].scrollIntoView({ block: 'nearest', behavior: 'smooth' });
                },
                updateSelected(value) {
                    this.selected = value;
                    this.autocomplete = value;
                    this.open = false;
                    this.highlightedIndex = 0;
                },
            }"
    >
        <div x-on:value-selected="updateSelected($event.detail)">
            <span>
                <x-input
                        id="{{ $id }}"
                        type="{{ $type }}"
                        name="search"
                        placeholder="Search for items"
                        class="border-2 border-gray-300 bg-white h-10 px-5 pr-16 rounded-lg text-sm focus:outline-none w-full"
                        x-on:keydown.arrow-down.stop.prevent="highlightNext()"
                        x-on:keydown.arrow-up.stop.prevent="highlightPrevious()"
                        x-on:keydown.enter.stop.prevent="$dispatch('value-selected', {resultName: $refs.results.children[highlightedIndex].getAttribute('data-result-name')})"
                        wire:model.live="autocomplete"
                        {{ $attributes }}
                />
            </span>
            <div x-show="open" x-on:click.away="open = false">
                <ul x-ref="results">
                    @forelse($results as $index => $result)
                        <li
                                {{--                                wire:key="{{ $index }}"--}}
                                {{--                                x-on:click.stop="$dispatch('value-selected', {--}}
                                {{--                                    id: {{ $result->id }},--}}
                                {{--                                    name: '{{ $result->name }}'--}}
                                {{--                                })"--}}
                                {{--                                :class="{--}}
                                {{--                                    'bg-indigo-400': {{ $index }} === highlightedIndex,--}}
                                {{--                                    'text-white': {{ $index }} === highlightedIndex-}}
                                {{--                                }"--}}
                                {{--                                data-result-id="{{ $result->id }}"--}}
                                {{--                                data-result-name="{{ $result->name }}"--}}
                        >
                            {{ $result }}
                            {{--                            <span>{{ $result->name }}</span>--}}
                        </li>
                    @empty
                        <li>No results found</li>
                    @endforelse
                </ul>
            </div>
        </div>
    </div>
</div>
