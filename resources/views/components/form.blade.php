<form method="POST" {{ $attributes }}>
    @method($method)
    @csrf
    <div class="shadow sm:rounded-md">
        <div class="px-4 py-5 bg-white sm:p-6">
            <div class="grid">
                {{ $form }}
            </div>
        </div>
        <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
            @if ($cancelUrl !== null)
                <x-beto::secondary-button href="{{ url($cancelUrl) }}">@lang('beto::common.actions.cancel')</x-beto::secondary-button>
            @endif
            <x-button>@lang('beto::common.actions.save')</x-button>
        </div>
    </div>
</form>
