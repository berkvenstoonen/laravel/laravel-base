import { Alpine } from '../../../../livewire/livewire/dist/livewire.esm';
(factory => {
    factory(Alpine);
})(Alpine => {
    let activeAutoLoader;

    const checkActiveAutoLoader = () => {
        if (activeAutoLoader !== undefined) {
            activeAutoLoader.checkToLoad();
        }
    };

    const elementInViewport = element => {
        let top = element.offsetTop;
        let left = element.offsetLeft;
        const width = element.offsetWidth;
        const height = element.offsetHeight;

        while (element.offsetParent) {
            element = element.offsetParent;
            top += element.offsetTop;
            left += element.offsetLeft;
        }

        return (
            top < (window.pageYOffset + window.innerHeight) &&
            left < (window.pageXOffset + window.innerWidth) &&
            (top + height) > window.pageYOffset &&
            (left + width) > window.pageXOffset
        );
    };

    Alpine.data('loadMoreContainer', () => ({
        noMore() {
            window.removeEventListener('scroll', checkActiveAutoLoader);
            window.removeEventListener('resize', checkActiveAutoLoader);
            activeAutoLoader = undefined;
        },
    }));

    Alpine.data('loadMoreBlock', () => {
        if (activeAutoLoader === undefined) {
            window.addEventListener('scroll', checkActiveAutoLoader);
            window.addEventListener('resize', checkActiveAutoLoader);
        }
        activeAutoLoader = {
            loaded: false,
            checkToLoad() {
                if (elementInViewport(this.$el)) {
                    this.$wire.call('load');
                }
            },
        };
        return activeAutoLoader;
    });
});
