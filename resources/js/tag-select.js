window.tagSelect = function () {
    return {
        open: false,
        showCreate: false,
        textInput: '',
        selected: [],
        tags: [],
        found: [],
        init() {
            this.selected = JSON.parse(this.$el.parentNode.getAttribute('data-selected'));
            this.tags = JSON.parse(this.$el.parentNode.getAttribute('data-tags'));
        },
        addTag(tag) {
            tag = tag.trim();
            if (tag !== '' && !this.hasTagSelected(tag)) {
                this.selected.push(tag);
            }
            this.$refs.textInput.focus();
            this.fireTagsUpdateEvent();
            setTimeout(() => {
                this.clearSearch();
            });
        },
        selectTag(query, event) {
            if (this.open && event !== undefined) {
                event.preventDefault();
                event.stopPropagation();
            }
            if (query === '') {
                return;
            }
            const found = this.tags.find(tag => !this.selected.includes(tag) && tag.toLowerCase().startsWith(query.toLowerCase()));
            if (found !== undefined) {
                this.addTag(found);
            }
        },
        selectOrAddTag(query) {
            const $currentFocus = this.$el.querySelector('.focussed');
            if ($currentFocus !== null) {
                this.addTag($currentFocus.innerText);
                return;
            }
            const found = this.tags.find(tag => !this.selected.includes(tag) && tag.toLowerCase().startsWith(query.toLowerCase()));
            this.addTag(found || query);
        },
        focusNext() {
            if (!this.open) {
                this.search('');
            }
            const $currentFocus = this.$el.querySelector('.focussed');
            if ($currentFocus !== null) {
                $currentFocus.classList.remove('focussed');
                const $next = $currentFocus.nextElementSibling;
                if ($next !== null && !$next.classList.contains('hidden')) {
                    $next.classList.add('focussed');
                    return;
                }
            }
            this.$el.querySelector('.tag-completion:first-of-type').classList.add('focussed');
        },
        focusPrevious() {
            if (!this.open) {
                this.search('');
            }
            const $currentFocus = this.$el.querySelector('.focussed');
            if ($currentFocus !== null) {
                $currentFocus.classList.remove('focussed');
                const $previous = $currentFocus.previousElementSibling;
                if ($previous !== null && !$previous.classList.contains('hidden') && $previous.classList.contains('tag-completion')) {
                    $previous.classList.add('focussed');
                    return;
                }
            }
            let $last = this.$el.querySelector('.tag-completion:last-of-type');
            if ($last.classList.contains('hidden')) {
                $last = $last.previousElementSibling;
            }
            $last.classList.add('focussed');
        },
        fireTagsUpdateEvent() {
            this.$el.dispatchEvent(new CustomEvent('tag-selection-updated', {
                detail: { selected: this.selected },
                bubbles: true,
            }));
        },
        hasTagSelected(tag) {
            return this.selected.find(selectedTag => selectedTag.toLowerCase() === tag.toLowerCase()) !== undefined;
        },
        removeTag(index) {
            this.selected.splice(index, 1);
            this.fireTagsUpdateEvent();
        },
        search(query) {
            if (query.includes(',')) {
                query.split(',').forEach(tag => this.addTag(tag.trim()));
            }
            this.found = this.tags.filter(tag => !this.selected.includes(tag) && tag.toLowerCase().startsWith(query.toLowerCase()));
            this.showCreate = query !== '' && !this.tags.includes(query);
            this.toggleSearch();
        },
        clearSearch(event) {
            if (event !== undefined && this.open) {
                event.stopPropagation();
                event.preventDefault();
            }
            this.textInput = '';
            this.open = false;
            const focussed = this.$el.querySelector('.focussed');
            if (focussed !== null) {
                focussed.classList.remove('focussed');
            }
        },
        toggleSearch() {
            this.open = document.activeElement === this.$refs.textInput && (this.found.length || this.showCreate);
        }
    };
};
