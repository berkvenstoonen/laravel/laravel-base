(() => {
    const darkModeIsForced = document.documentElement.classList.contains('dark') || document.documentElement.classList.contains('light');

    if (!darkModeIsForced) {
        const theme = localStorage.getItem('theme');
        if (theme !== null) {
            document.documentElement.classList.toggle('dark', theme === 'dark');
        } else {
            document.documentElement.classList.toggle('dark', window.matchMedia('(prefers-color-scheme: dark)').matches);
        }
    }

    if (document.documentElement.classList.contains('dark')) {
        document.querySelector('meta[name="image-error-url"]').content = document.querySelector('meta[name="dark-image-error-url"]').content;
    }
    if (document.documentElement.classList.contains('light')) {
        document.documentElement.classList.remove('light');
    }
    document.querySelector('meta[name="dark-image-error-url"]').remove();

    const listener = () => {
        document.documentElement.classList.toggle('dark');
        elements.forEach($el => $el.checked = document.documentElement.classList.contains('dark'));
        localStorage.setItem('theme', document.documentElement.classList.contains('dark') ? 'dark' : 'light');
    };

    const elements = [];
    window.toggleDarkMode = function () {
        return {
            init() {
                elements.push(this.$el);
                this.$el.removeEventListener('change', listener);
                this.$el.addEventListener('change', listener);
                if (!this.$el.checked && document.documentElement.classList.contains('dark')) {
                    this.$el.checked = true;
                }
            },
        };
    };
})();
