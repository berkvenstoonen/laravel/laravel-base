import jQuery from 'jquery';
import select2 from 'select2'
const callbacks = [];
const betoSelect2 = {
    updateOptionsHook: callback => callbacks.push(callback),
};
export { betoSelect2 as betoSelect2 }
(factory => {
    betoSelect2.init = Alpine => factory(jQuery, Alpine);
})(($, Alpine) => {
    select2($);
    Alpine.data('select2', () => ({
        init() {
            const $element = $(this.$el);
            const options = {
                selectionCssClass: ':all:',
                tags: $element.data('allow-custom'),
                placeholder: $element.data('placeholder'),
                templateResult: state => {
                    if (state.html === undefined) {
                        return state.text;
                    }
                    return $('<span>' + state.html + '</span>');
                }
            };
            callbacks.forEach(callback => callback($element, options));
            $element.select2(options);
            $element.on('change', () => {
                const data = $element.select2('val');
                this.$wire.set($element.data('wire-model'), data);
            });
        },
    }));
});
