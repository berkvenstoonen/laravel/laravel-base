<?php

use BeTo\Laravel\Listeners\Build;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Build::setNeedsBuild();
    }
};
