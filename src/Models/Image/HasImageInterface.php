<?php
declare(strict_types=1);

namespace BeTo\Laravel\Models\Image;

use BeTo\Laravel\Enums\ThumbnailSize;

interface HasImageInterface
{
    public function hasImage(): bool;

    public function getFullUrl(): string;

    public function getThumbnailUrl(ThumbnailSize $size): string;
}
