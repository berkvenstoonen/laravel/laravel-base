<?php
declare(strict_types=1);

namespace BeTo\Laravel\Models\Image;

use BeTo\Laravel\Enums\ThumbnailSize;

interface HasImageFromFileInterface extends HasImageInterface
{
    public function getImagePath(): string;

    public function getThumbnailPath(ThumbnailSize $size): string;

    public function getVersion(): int;
}
