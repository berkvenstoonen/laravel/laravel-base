<?php
declare(strict_types=1);

namespace BeTo\Laravel\Models\Image;

use BeTo\Laravel\Enums\ThumbnailSize;
use BeTo\Laravel\Helpers\Thumbnail;

trait ImageFromFileTrait
{
    public function getThumbnailUrl(ThumbnailSize $size): string
    {
        return Thumbnail::getThumbnailUrl($this, $size);
    }

    public function getVersion(): int
    {
        return $this->updated_at->timestamp;
    }
}
