<?php
declare(strict_types=1);

namespace BeTo\Laravel\Models;

use App\Models\Team;
use BeTo\Laravel\Exceptions\ProgrammingException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Auth\User;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Laravel\Jetstream\HasTeams;
use Sentry\Serializer\SerializableInterface;

/**
 * \BeTo\LaravelSso\Models\BaseUser
 *
 * @property int                                                        $id
 * @property string                                                     $name
 * @property string                                                     $email
 * @property int|null                                                   $current_team_id
 * @property string|null                                                $profile_photo_path
 * @property Carbon|null                                                $created_at
 * @property Carbon|null                                                $updated_at
 * @property bool|null                                                  $dark_mode
 * @property string                                                     $locale
 * @property-read Team|null                                             $currentTeam
 * @property-read string                                                $profile_photo_url
 * @property-read DatabaseNotificationCollection|DatabaseNotification[] $notifications
 * @property-read int|null                                              $notifications_count
 * @property-read Collection|Team[]                                     $ownedTeams
 * @property-read int|null                                              $owned_teams_count
 * @property-read Collection|Team[]                                     $teams
 * @property-read int|null                                              $teams_count
 * @method static Builder|static newModelQuery()
 * @method static Builder|static newQuery()
 * @method static Builder|static query()
 * @method static Builder|static whereCreatedAt($value)
 * @method static Builder|static whereCurrentTeamId($value)
 * @method static Builder|static whereDarkMode($value)
 * @method static Builder|static whereEmail($value)
 * @method static Builder|static whereId($value)
 * @method static Builder|static whereLocale($value)
 * @method static Builder|static whereName($value)
 * @method static Builder|static whereProfilePhotoPath($value)
 * @method static Builder|static whereUpdatedAt($value)
 * @mixin \Eloquent
 */
abstract class BaseUser extends User implements SerializableInterface
{
    use HasTeams;
    use Notifiable;

    protected $fillable = [
        'name',
        'email',
        'locale',
        'dark_mode',
    ];

    protected $casts = [
        'dark_mode' => 'boolean',
    ];

    public function getDarkModeClass(): string
    {
        return match ($this->dark_mode) {
            null  => '',
            true  => 'dark',
            false => 'light',
        };
    }

    public static function getCurrent(): static
    {
        $user = self::tryGetCurrent();
        if ($user === null) {
            throw new ProgrammingException('There is no current user');
        }
        return $user;
    }

    public static function tryGetCurrent(): ?static
    {
        /** @var null|static $user */
        $user = Auth::user();
        if (!$user?->exists) {
            return null;
        }
        return $user;
    }

    public static function hasCurrent(): bool
    {
        $user = Auth::user();
        return (bool) $user?->exists;
    }

    public static function allowedToView(self $user = null): Collection
    {
        if ($user === null) {
            $user = self::tryGetCurrent();
        }
        $users = static::whereHas('teams', static function (Builder $builder) use ($user) {
            $teamIds = $user->allTeams()->pluck('id')->all();
            $builder->whereIn('teams.id', $teamIds);
        })->get();
        foreach ($user->teams as $team) {
            $users->add($team->owner);
        }
        return $users;
    }

    public function toSentry(): array
    {
        return $this->toArray();
    }
}
