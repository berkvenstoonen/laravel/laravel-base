<?php
declare(strict_types=1);

namespace BeTo\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\LazyCollection;
use Illuminate\Support\Str;
use Sentry\Serializer\SerializableInterface;

/**
 * @property int $id
 * @method static static create(array $data)
 * @mixin \Eloquent
 */
abstract class BaseModel extends Model implements SerializableInterface
{
    /**
     * @return array<string, mixed>
     */
    public function toSentry(): array
    {
        return $this->toArray();
    }

    /**
     * This function is a basic implementation for the @see AppSearchEntityInterface.
     * @return LazyCollection<int, static>
     */
    public static function getIndexableEntities(): iterable
    {
        return static::lazy(100);
    }

    /**
     * This function is a basic implementation for the @see AppSearchEntityInterface.
     */
    public static function getEngineName(): string
    {
        if (defined('static::INDEX_NAME')) {
            return static::INDEX_NAME;
        }
        $parts = explode('\\', static::class);
        return Str::kebab($parts[array_key_last($parts)]).'s';
    }

    /**
     * This function is a basic implementation for the @see AppSearchEntityInterface.
     */
    public function getAppSearchId(): string
    {
        $parts = explode('\\', static::class);
        return Str::kebab($parts[array_key_last($parts)]).'-'.$this->id;
    }
}
