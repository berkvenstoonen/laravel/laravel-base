<?php

namespace BeTo\Laravel\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Translation\PotentiallyTranslatedString;

final readonly class AllowedEmail implements ValidationRule
{
    /**
     * Run the validation rule.
     *
     * @param Closure(string): PotentiallyTranslatedString $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        if (!is_string($value)) {
            $fail(trans('validation.string', ['attribute' => $value]));
        }
        if (config('app.env') === 'testing') {
            return;
        }
        $allowedDomains        = config('user-management.accountLimitations.allowedDomains', []);
        $allowedEmailAddresses = config('user-management.accountLimitations.allowedEmailAddresses', []);
        if (empty($allowedDomains) && empty($allowedEmailAddresses)) {
            return;
        }
        foreach ($allowedDomains as $allowedDomain) {
            if (str_ends_with($value, $allowedDomain)) {
                return;
            }
        }
        foreach ($allowedEmailAddresses as $allowedEmailAddress) {
            if (strtolower($value) === strtolower($allowedEmailAddress)) {
                return;
            }
        }
        $fail(trans('beto::validation.allowed_email', ['attribute' => $value]));
    }
}
