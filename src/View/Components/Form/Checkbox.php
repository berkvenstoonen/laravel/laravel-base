<?php

declare(strict_types=1);

namespace BeTo\Laravel\View\Components\Form;

use Illuminate\Support\Facades\Lang;
use Illuminate\View\Component;
use Webmozart\Assert\Assert;

final class Checkbox extends Component
{
    public string $id;
    public string $label;
    public bool $indeterminate;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $id, string $label = null, bool $indeterminate = false)
    {
        $label = trans($label ?? (Lang::has('common.fields.' . $id) ? 'common.fields.' . $id : 'beto::common.fields.' . $id));
        Assert::string($label);
        $this->id            = $id;
        $this->label         = $label;
        $this->indeterminate = $indeterminate;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('beto::components.form.checkbox');
    }
}
