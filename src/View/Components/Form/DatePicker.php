<?php

declare(strict_types=1);

namespace BeTo\Laravel\View\Components\Form;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Lang;
use Illuminate\View\Component;
use Webmozart\Assert\Assert;

final class DatePicker extends Component
{
    public string $label;

    public function __construct(
        public string $id,
        public string $type = 'text',
        string $label = null,
        public string | null $tooltip = null,
        public bool $fullWidth = true,
        public bool $padding = true,
    ) {
        $label = trans($label ?? (Lang::has('common.fields.' . $id) ? 'common.fields.' . $id : 'beto::common.fields.' . $id));
        Assert::string($label);
        $this->label = $label;
    }

    public function render(): View
    {
        return view('beto::components.form.date-picker');
    }
}
