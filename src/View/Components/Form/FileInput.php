<?php

declare(strict_types=1);

namespace BeTo\Laravel\View\Components\Form;

use Illuminate\View\Component;

final class FileInput extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('beto::components.form.file-input');
    }
}
