<?php

declare(strict_types=1);

namespace BeTo\Laravel\View\Components\Form;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Lang;
use Illuminate\View\Component;
use Webmozart\Assert\Assert;

class AutocompleteInput extends Component
{
    public string $label;

    /**
     * Create a new component instance.
     *
     * @param array<string> $results
     * @return void
     */
    public function __construct(
        public string $id,
        public array $results,
        public string $type = 'text',
        public bool $fullWidth = true,
        string $label = null,
    ) {
        $label = trans($this->parseLabel($label, $id));
        Assert::string($label);
        $this->label = $label;
    }

    private function parseLabel(?string $label, string $id): string
    {
        if ($label !== null) {
            return $label;
        }
        if (Lang::has('common.fields.' . $id)) {
            return 'common.fields.' . $id;
        } elseif (Lang::has('beto::common.fields.' . $id)) {
            return 'beto::common.fields.' . $id;
        }
        return $id;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View
    {
        return view('beto::components.form.autocomplete-input', ['results' => []]);
    }
}
