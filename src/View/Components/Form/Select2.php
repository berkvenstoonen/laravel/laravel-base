<?php

declare(strict_types=1);

namespace BeTo\Laravel\View\Components\Form;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Lang;
use Illuminate\View\Component;
use Webmozart\Assert\Assert;

class Select2 extends Component
{
    /** @var array<array<string, (int|string)>> */
    public array  $tags = [];
    public string $label;

    /**
     * @param int|array<int> $selected
     * @param array<string> $tags
     */
    public function __construct(
        public string          $id,
        public int|array       $selected,
        Collection|array       $tags,
        public ?string         $autocompleteUrl = null,
        public bool            $multiple = true,
        public bool            $allowCustom = true,
        public bool            $named = false,
        public readonly int    $autocompleteLength = 2,
        public readonly array  $autocompleteHeaders = ['Content-Type' => 'application/json'],
        public readonly array  $autocompleteFilters = [],
        string                 $label = null,
    ) {
        foreach ($tags as $tagId => $tag) {
            $this->tags[] = [
                'id'   => $tagId,
                'text' => $tag,
            ];
        }
        $label = trans($label ?? (Lang::has('common.fields.' . $id) ? 'common.fields.' . $id : 'beto::common.fields.' . $id));
        Assert::string($label);
        $this->label = $label;
    }

    public function render(): View
    {
        return view('beto::components.form.select2');
    }
}
