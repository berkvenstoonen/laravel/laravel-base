<?php

declare(strict_types=1);

namespace BeTo\Laravel\View\Components\Form;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Lang;
use Illuminate\View\Component;
use Webmozart\Assert\Assert;

final class SelectInput extends Component
{
    public string $label;

    /**
     * Create a new component instance.
     *
     * @param array<string> $values
     * @return void
     */
    public function __construct(
        public string $id,
        public array $values,
        public bool $fullWidth = true,
        public bool $empty = false,
        ?string $label = null,
    ) {
        $label = trans($label ?? (Lang::has('common.fields.' . $id) ? 'common.fields.' . $id : 'beto::common.fields.' . $id));
        Assert::string($label);
        $this->label = $label;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View
    {
        return view('beto::components.form.select-input');
    }
}
