<?php

declare(strict_types=1);

namespace BeTo\Laravel\View\Components\Form;

use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Modal extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        public string $method = 'POST',
        public ?string $cancelUrl = null,
    ) {
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View
    {
        return view('beto::components.form.modal');
    }
}
