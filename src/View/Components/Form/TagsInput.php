<?php

declare(strict_types=1);

namespace BeTo\Laravel\View\Components\Form;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Lang;
use Illuminate\View\Component;
use Webmozart\Assert\Assert;

class TagsInput extends Component
{
    /** @var array<int>  */
    public array $tags;
    /** @var array<int>  */
    public array $selected;
    public string $label;

    /**
     * Create a new component instance.
     *
     * @param  array<int>|string $tags
     * @param  array<int>|string $selected
     * @return void
     */
    public function __construct(
        public string $id,
        array|string  $tags = [],
        array|string  $selected = [],
        string        $label = null,
        public bool   $fullWidth = true
    ) {
        $tags     = is_array($tags) ? $tags : json_decode($tags, true, flags: JSON_THROW_ON_ERROR);
        Assert::isArray($tags);
        $selected = is_array($selected) ? $selected : json_decode($selected, true, flags: JSON_THROW_ON_ERROR);
        Assert::isArray($selected);
        $label    = trans($label ?? (Lang::has('common.fields.' . $id) ? 'common.fields.' . $id : 'beto::common.fields.' . $id));
        Assert::string($label);
        $this->tags     = $tags;
        $this->selected = $selected;
        $this->label    = $label;
        sort($this->tags);
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View
    {
        return view('beto::components.form.tags-input');
    }
}
