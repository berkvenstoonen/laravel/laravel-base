<?php

declare(strict_types=1);

namespace BeTo\Laravel\View\Components;

use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

final class CopyToClipboardButton extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        public readonly string $copyText,
    ) {
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View
    {
        return view('beto::components.copy-to-clipboard-button');
    }
}
