<?php
declare(strict_types=1);

namespace BeTo\Laravel\Http\Livewire;

use Illuminate\Support\Collection;
use Illuminate\View\View;
use Livewire\Component;

abstract class LoadMoreBlock extends Component
{
    public array      $filters    = [];
    public int        $page       = 0;
    public int        $totalPages = 0;
    public bool       $loaded     = false;
    public Collection $items;
    /** @var array<string> */
    protected $listeners = ['search'];

    public function load(): void
    {
        $this->loaded = true;
    }

    public function search(array $filters): void
    {
        $this->filters = $filters;
    }

    public function render(): View
    {
        if ($this->loaded) {
            $this->items      = $this->getItems();
            $this->totalPages = $this->getTotalPages();
        }
        return view('beto::livewire.load-more-block');
    }

    abstract protected function getItems(): Collection;

    abstract protected function getTotalPages(): int;
}
