<?php

declare(strict_types=1);

namespace BeTo\Laravel\Http\Livewire;

trait SetValueTrait
{
    public function setValue(string $model, array $value): void
    {
        if (!str_contains($model, '.')) {
            $this->$model = $value;
            return;
        }
        $model = explode('.', $model);
        if (count($model) > 2) {
            throw new \Exception('Not yet implemented to support more than 1 nesting');
        }
        $property                      = array_shift($model);
        $propertySub                   = array_shift($model);
        $this->$property[$propertySub] = $value;
    }
}
