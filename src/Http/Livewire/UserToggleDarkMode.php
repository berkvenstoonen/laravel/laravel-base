<?php
declare(strict_types=1);

namespace BeTo\Laravel\Http\Livewire;

use App\Models\User;
use Illuminate\View\View;
use Livewire\Component;

final class UserToggleDarkMode extends Component
{
    public User $user;
    public ?bool $darkMode;
    protected array $rules = [
        'dark_mode' => ['required', 'bool'],
    ];

    public function mount(User $user): void
    {
        $this->user = $user;
        $this->darkMode = $user->dark_mode;
    }

    public function updatedDarkMode(): void
    {
        $this->user->update(['dark_mode' => $this->darkMode]);
    }

    public function render(): View
    {
        return view('beto::components.user-toggle-dark-mode');
    }
}
