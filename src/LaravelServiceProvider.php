<?php
declare(strict_types=1);

namespace BeTo\Laravel;

use BeTo\Laravel\Console\Commands\MakeBuildMigration;
use BeTo\Laravel\Console\Commands\MakeModelControllers;
use BeTo\Laravel\Console\Commands\Test;
use BeTo\Laravel\Exceptions\Handler;
use BeTo\Laravel\Http\Livewire\UserToggleDarkMode;
use BeTo\Laravel\Listeners\Build;
use BeTo\Laravel\View\Components\Form;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Database\Events\MigrationsEnded;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\ServiceProvider;
use Livewire\Livewire;

final class LaravelServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/beto.php', 'beto');
        $this->commands(
            [
                Test::class,
                MakeBuildMigration::class,
                MakeModelControllers::class,
                Console\Commands\Build::class,
                Console\Commands\CreateDoTest::class,
                Console\Commands\PrepareNodeModules::class,
            ],
        );
    }

    public function boot(): void
    {
        $this->initViewComponents();
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->publishes([__DIR__ . '/../config/beto.php' => config_path('beto.php')], 'config');
        $this->publishes([__DIR__ . '/../resources/js' => resource_path('js')], 'config');
        $this->loadTranslationsFrom(__DIR__ . '/../lang', 'beto');
        $this->app->singleton(
            ExceptionHandler::class,
            Handler::class,
        );
        Event::listen(MigrationsEnded::class, [Build::class, 'handle']);
    }

    private function initViewComponents(): void
    {
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'beto');
        $this->publishes([__DIR__ . '/../resources/views' => resource_path('views/vendor/beto')]);
        Blade::component('beto-form', Form::class);
        Blade::component('beto-form.autocomplete-input', Form\AutocompleteInput::class);
        Blade::component('beto-form.checkbox', Form\Checkbox::class);
        Blade::component('beto-form.file-input', Form\FileInput::class);
        Blade::component('beto-form.input', Form\Input::class);
        Blade::component('beto-form.modal', Form\Modal::class);
        Blade::component('beto-form.select2', Form\Select2::class);
        Blade::component('beto-form.select-input', Form\SelectInput::class);
        Blade::component('beto-form.tags-input', Form\TagsInput::class);
        Blade::component('beto-form.date-picker', Form\DatePicker::class);
        Blade::component('beto-form.textarea', Form\Textarea::class);
        Livewire::component('beto::user-toggle-dark-mode', UserToggleDarkMode::class);
    }
}
