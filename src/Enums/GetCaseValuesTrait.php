<?php

declare(strict_types=1);

namespace BeTo\Laravel\Enums;

trait GetCaseValuesTrait
{
    public static function caseValues(): array
    {
        return array_column(self::cases(), 'value');
    }
}
