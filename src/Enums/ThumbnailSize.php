<?php

declare(strict_types=1);

namespace BeTo\Laravel\Enums;

enum ThumbnailSize: int
{
    use GetCaseValuesTrait;

    case Small = 200;
    case Medium = 350;
    case Large = 600;
}
