<?php
declare(strict_types=1);

namespace BeTo\Laravel\Constants;

final class Validation
{
    public const IMAGE_MIMES = 'mimes:jpg,jpeg,png,webp';

    private function __construct()
    {
    }
}
