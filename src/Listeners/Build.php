<?php

namespace BeTo\Laravel\Listeners;

use BeTo\Laravel\Console\ExecuteShellCommand;
use Illuminate\Console\OutputStyle;
use Illuminate\Console\View\Components\Info;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Output\ConsoleOutput;

final class Build
{
    private static bool $needsBuild = false;

    public static function setNeedsBuild(): void
    {
        self::$needsBuild = true;
    }

    public function handle(): void
    {
        if (!self::$needsBuild || !config('theme.useBuild', config('app.env') === 'production')) {
            return;
        }

        \BeTo\Laravel\Helpers\Build::build();
    }
}
