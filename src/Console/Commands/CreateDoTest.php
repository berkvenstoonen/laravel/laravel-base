<?php

declare(strict_types=1);

namespace BeTo\Laravel\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand('c', 'Create the DoTest class')]
final class CreateDoTest extends Command
{
    public function handle(): int
    {
        $newFile = '<?php
declare(strict_types=1);

namespace BeTo\Laravel\Console\Commands;

use Symfony\Component\Console\Output\OutputInterface;

final readonly class DoTest
{
    public static function test(OutputInterface $output): void
    {
    }
}
';
        file_put_contents(__DIR__ . DIRECTORY_SEPARATOR . 'DoTest.php', $newFile);
        return 0;
    }
}
