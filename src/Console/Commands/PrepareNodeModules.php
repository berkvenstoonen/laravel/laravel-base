<?php

declare(strict_types=1);

namespace BeTo\Laravel\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Attribute\AsCommand;

#[AsCommand('prepare:node-modules', 'Get the node-modules and add them to the packages.json')]
final class PrepareNodeModules extends Command
{
    /**
     * Execute the console command.
     */
    public function handle(): int
    {
        $vendorDir       = realpath(__DIR__ . '/../../../../../');
        $devDependencies = [];
        $dependencies    = [];
        foreach (glob($vendorDir . '/*/*/composer.json') as $composerFile) {
            $composer = json_decode(file_get_contents($composerFile), associative: true, flags: JSON_THROW_ON_ERROR);
            if (array_key_exists('extra', $composer) && array_key_exists('npm', $composer['extra'])) {
                if (array_key_exists('dev', $composer['extra']['npm'])) {
                    $devDependencies += $composer['extra']['npm']['dev'];
                    unset($composer['extra']['npm']['dev']);
                }
                $dependencies += $composer['extra']['npm'];
            }
        }

        $packagesFile = realpath($vendorDir . '/../') . '/package.json';
        $packages     = json_decode(file_get_contents($packagesFile), associative: true, flags: JSON_THROW_ON_ERROR);
        if (!empty($devDependencies)) {
            if (!array_key_exists('devDependencies', $packages)) {
                $packages['devDependencies'] = [];
            }
            $packages['devDependencies'] += $devDependencies;
            ksort($packages['devDependencies']);
        }
        if (!empty($dependencies)) {
            if (!array_key_exists('dependencies', $packages)) {
                $packages['dependencies'] = [];
            }
            $packages['dependencies'] += $dependencies;
            ksort($packages['dependencies']);
        }
        file_put_contents($packagesFile, json_encode($packages, flags: JSON_PRETTY_PRINT));
        return 0;
    }
}
