<?php

declare(strict_types=1);

namespace BeTo\Laravel\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Attribute\AsCommand;

#[AsCommand('make:build', 'Completely (re)build the css and js assets')]
final class Build extends Command
{
    /**
     * Execute the console command.
     */
    public function handle(): int
    {
        \BeTo\Laravel\Helpers\Build::build();
        return 0;
    }
}
