<?php

declare(strict_types=1);

namespace BeTo\Laravel\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Symfony\Component\Console\Question\ConfirmationQuestion;

final class MakeModelControllers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:model-controller {model} {--without-search}';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command creates a controller, requests, views, and everything else needed to simply manage a model.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle(): int
    {
        $modelName = $this->argument('model');
        if (!class_exists('\\App\\Models\\' . $modelName)) {
            $this->output->writeln('<error>Cannot find class <comment>\\App\\Models\\' . $modelName . '</comment></error>');
            if (!$this->output->askQuestion(new ConfirmationQuestion('<question>Do you want to create the model</question>?', false))) {
                return 1;
            }
            $this->call('make:model', ['name' => $modelName, '--migration' => true]);
            $this->call('make:policy', ['name' => $modelName.'Policy', '--model' => $modelName]);
        }
        $this->createLivewireComponents($modelName);
        $this->createSimpleComponents($modelName);
        $this->addRoutes($modelName);
        return 0;
    }

    private function createLivewireComponents(string $modelName): void
    {
        $useSearch = !$this->option('without-search') && class_exists('\BeTo\LaravelElasticAppSearch\Http\Livewire\SearchForm');
        $suffix    = $useSearch ? '-search' : '';
        $this->createView($modelName, 'index', $suffix);
        $this->createView($modelName, 'create');
        $this->createView($modelName, 'view');
        $this->createView($modelName, 'edit');
    }

    private function createView(string $modelName, string $type, string $stubsSuffix = ''): void
    {
        $folderName = Str::snake($modelName, '-');
        $this->replaceStubs($modelName, base_path('stubs/livewire/') . $type . $stubsSuffix . '/livewire.stub', base_path('stubs/tmp/') . 'livewire.stub');
        $this->replaceStubs($modelName, base_path('stubs/livewire/') . $type . $stubsSuffix . '/livewire.view.stub', base_path('stubs/tmp/') . 'livewire.view.stub');
        $this->call('make:livewire', ['name' => $folderName . '.' . $type, '--stub' => 'tmp']);
    }

    private function replaceStubs(string $modelName, string $file, string $tmpFile): void
    {
        $snakeCased    = Str::snake($modelName, '-');
        $stubVariables = [
            '[model-route-prefix]' => $snakeCased,
            '[model-view]'         => $snakeCased,
            '[model-class]'        => '\\App\\Models\\' . $modelName,
        ];
        file_put_contents($tmpFile, str_replace(array_keys($stubVariables), array_values($stubVariables), file_get_contents($file)));
    }

    private function createSimpleComponents(string $modelName): void
    {
        $this->createSimpleComponent($modelName, 'tile');
        $this->createSimpleComponent($modelName, 'image');
    }

    private function createSimpleComponent(string $modelName, string $fileName): void
    {
        $className     = Str::studly($fileName);
        $snakeCased    = Str::snake($modelName, '-');
        $stubVariables = [
            '[model-route-prefix]' => $snakeCased,
            '[model-view]'         => $snakeCased,
            '[model-class]'        => '\\App\\Models\\' . $modelName,
            '[class]'              => $className,
            '[namespace]'          => 'App\\View\\Components\\' . $modelName,
        ];
        $this->call('make:component', ['name' => $modelName . DIRECTORY_SEPARATOR . Str::studly($fileName)]);
        $stubsFile = base_path('stubs/components') . DIRECTORY_SEPARATOR . $fileName . '.view.stub';
        $viewFile  = resource_path('views/components/' . $snakeCased) . DIRECTORY_SEPARATOR . $fileName . '.blade.php';
        file_put_contents($viewFile, str_replace(array_keys($stubVariables), array_values($stubVariables), file_get_contents($stubsFile)));

        $stubsFile = base_path('stubs/components') . DIRECTORY_SEPARATOR . $fileName . '.stub';
        $classFile = app_path('View/Components/' . $modelName) . DIRECTORY_SEPARATOR . Str::studly($fileName) . '.php';
        file_put_contents($classFile, str_replace(array_keys($stubVariables), array_values($stubVariables), file_get_contents($stubsFile)));
    }

    private function addRoutes(string $modelName): void
    {
        $snakeCased    = Str::snake($modelName, '-');
        $stubVariables = [
            '[name-prefix]'          => $snakeCased,
            '[url-prefix]'           => Str::plural($snakeCased),
            '[controller-namespace]' => '\\App\\Http\\Livewire\\' . $modelName,
            '[model-class]'          => '\\App\\Models\\' . $modelName,
        ];
        $routesFile    = base_path('routes') . '/web.php';
        $stubsFile     = base_path('stubs') . '/model-routes.stub';
        $routes        = str_replace(array_keys($stubVariables), array_values($stubVariables), file_get_contents($stubsFile));
        file_put_contents($routesFile, file_get_contents($routesFile) . $routes);
    }
}
