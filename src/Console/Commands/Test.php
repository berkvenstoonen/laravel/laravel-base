<?php

declare(strict_types=1);

namespace BeTo\Laravel\Console\Commands;

use BeTo\Laravel\Helpers\Timer;
use Illuminate\Console\Command;
use Symfony\Component\Console\Attribute\AsCommand;

#[AsCommand('t', 'This command is used to quick test things')]
final class Test extends Command
{
    public function handle(): int
    {
        $this->output->writeln('<info>Test Command</info>');
        Timer::create($this->output)->time(function () {
            DoTest::test($this->output);
        });
        return 0;
    }
}
