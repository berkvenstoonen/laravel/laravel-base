<?php

declare(strict_types=1);

namespace BeTo\Laravel\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;

final class MakeBuildMigration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:build-migration';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command creates a migration that triggers the build';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle(): int
    {
        $fileName = Carbon::now()->format('Y_m_d_His').'_build';
        $this->output->writeln('<info>Created Migration: </info>'.$fileName);
        file_put_contents(database_path('migrations').DIRECTORY_SEPARATOR.$fileName.'.php', file_get_contents(__DIR__.'/../../../database/migrations/build.php'));
        return 0;
    }
}
