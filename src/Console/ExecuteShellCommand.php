<?php

declare(strict_types=1);

namespace BeTo\Laravel\Console;

use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

final class ExecuteShellCommand
{
    /**
     * @param array<string|int|float> $command
     */
    public static function execute(array $command): void
    {
        $process = new Process($command);
        $process->setTimeout(3600);
        $process->run();
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
    }
}
