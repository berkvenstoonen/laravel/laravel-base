<?php

declare(strict_types=1);

namespace BeTo\Laravel\Exceptions;

use App\Models\User;
use BeTo\Laravel\Helpers\DoNotTrack;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Sentry\State\Scope;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Webmozart\Assert\Assert;

use function Sentry\configureScope;

class Handler extends ExceptionHandler
{
    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    public static function reportBackground(\Throwable $exception): void
    {
        if (config('beto.throw_background_errors')) {
            throw new BackgroundException($exception);
        }
        /** @var Handler $exceptionHandler */
        $exceptionHandler = app(self::class);
        $exceptionHandler->logIfNeeded($exception);
    }

    /**
     * Register the exception handling callbacks for the application.
     */
    public function register(): void
    {
//        if (preg_match('/platforms\/[0-9]+\/login-iframe/', $_SERVER['REDIRECT_URL'] ?? '')) {
//            config(['session.same_site' => 'none']);
//        }
        if (self::sentryEnabled()) {
            configureScope(function (Scope $scope): void {
                $appName = config('app.name');
                Assert::string($appName);
                $scope->setTag('app.name', $appName);
                $currentUser = User::tryGetCurrent();
                if ($currentUser !== null) {
                    $scope->setUser($currentUser->toArray());
                }
            });
        }
        $this->reportable($this::logIfNeeded(...));

        $this->renderable(function (\Throwable $exception, Request $request) {
            if (!$request->is('api/*')) {
                return null;
            }
            if ($request->is('api/scim/*')) {
                return $this->getScimResponse($exception);
            }
            if (!$exception instanceof HttpException) {
                $exception = new HttpException(500, $exception->getMessage(), $exception);
            }
            return response()->json(['error' => ['message' => $exception->getMessage(), 'status_code' => $exception->getStatusCode()]], $exception->getStatusCode());
        });
    }

    public function logIfNeeded(\Throwable $exception): void
    {
        if (!$this->shouldLogToSentry($exception)) {
            return;
        }
        if ($exception instanceof ExtraDataInterface) {
            configureScope(function (Scope $scope) use ($exception): void {
                $scope->setExtras($exception->getExtraData());
            });
        }
        if ($exception instanceof MultipleExceptionsInterface) {
            configureScope(function (Scope $scope) use ($exception): void {
                $scope->setTag('GroupedExceptionId', $exception->getGroupedExceptionId());
            });
            foreach ($exception->getExtraExceptions() as $extraException) {
                app('sentry')->captureException($extraException);
            }
        }
        app('sentry')->captureException($exception);
    }

    public static function sentryEnabled(): bool
    {
        return (
            app()->bound('sentry')
            && config('beto.sentry.environment') !== null
            && !DoNotTrack::dnt()
        );
    }

    private function shouldLogToSentry(\Throwable $exception): bool
    {
        return self::sentryEnabled() && $this->shouldReport($exception);
    }

    private function getScimResponse(\Throwable $exception): JsonResponse
    {
        try {
            [$detail, $status] = match ($exception::class) {
                AuthenticationException::class => [$exception->getMessage(), 401],
                HttpException::class           => [$exception->getMessage(), $exception->getStatusCode()],
                default                        => throw $exception,
            };
        } catch (\Throwable $exception) {
            $detail = config('beto.throw_background_errors', false) ? $exception::class.' '.$exception->getMessage() : 'Unknown error';
            $status = 500;
        }
        return response()->json(
            [
                'schemas' => ['urn:ietf:params:scim:api:messages:2.0:Error'],
                'detail'  => $detail,
                'status'  => $status,
            ],
            $status
        );
    }

    protected function shouldntReport(\Throwable $e): bool
    {
        if (config('beto.sentry.log_all', false)) {
            return false;
        }
        return parent::shouldntReport($e);
    }
}
