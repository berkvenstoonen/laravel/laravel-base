<?php
declare(strict_types=1);

namespace BeTo\Laravel\Exceptions;

final class GetRelevantExceptionData
{
    public static function getRelevantData(\Throwable $exception, bool $allTraces = false): array
    {
        $data = [self::getRelevantDataForException($exception, $allTraces)];
        while ($previous = $exception->getPrevious()) {
            $data[] = self::getRelevantDataForException($previous, $allTraces);
        }
        return $data;
    }

    private static function getRelevantDataForException(\Throwable $exception, bool $allTraces = false): array
    {
        $relevantTraces = [];
        $projectPath    = str_replace('vendor/beto/laravel/src/Exceptions/GetRelevantExceptionData.php', '', __FILE__);
        if ($allTraces || self::isRelevantPath($projectPath, $exception->getFile())) {
            $relevantTraces[] = str_replace($projectPath, '', $exception->getFile()) . ':' . $exception->getLine();
        }
        foreach ($exception->getTrace() as $trace) {
            if (!array_key_exists('file', $trace)) {
                $relevantTraces[] = 'unknown trace: '.json_encode($trace);
            } elseif ($allTraces || self::isRelevantPath($projectPath, $trace['file'])) {
                $relevantTraces[] = str_replace($projectPath, '', $trace['file']) . ':' . $trace['line'];
            }
        }
        return [
            'message'        => $exception->getMessage(),
            'class'          => $exception::class,
            'code'           => $exception->getCode(),
            'relevantTraces' => $relevantTraces,
        ];
    }

    private static function isRelevantPath(string $projectPath, string $path): bool
    {
        return (
            str_starts_with($path, $projectPath . 'app')
            || str_starts_with($path, $projectPath . 'config')
            || str_starts_with($path, $projectPath . 'database')
            || str_starts_with($path, $projectPath . 'resources')
            || str_starts_with($path, $projectPath . 'routes')
            || str_starts_with($path, $projectPath . 'vendor/beto')
        );
    }
}
