<?php

declare(strict_types=1);

namespace BeTo\Laravel\Exceptions;

interface ExtraDataInterface
{
    /**
     * @return array<string, mixed>
     */
    public function getExtraData(): array;
}
