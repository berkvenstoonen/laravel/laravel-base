<?php

declare(strict_types=1);

namespace BeTo\Laravel\Exceptions;

class CommandErrorException extends BeToException implements ExtraDataInterface
{
    /**
     * @param array<string> $output
     */
    public function __construct(
        private readonly string $command,
        private readonly array  $output,
        private readonly ?int   $resultCode = null,
        \Throwable              $previous = null
    ) {
        parent::__construct('Problem executing command', $previous);
    }

    /**
     * @return array<string, string|array<string>|int|null>
     */
    public function getExtraData(): array
    {
        return [
            'command'    => $this->command,
            'output'     => $this->output,
            'resultCode' => $this->resultCode,
        ];
    }
}
