<?php

declare(strict_types=1);

namespace BeTo\Laravel\Exceptions;

class BackgroundException extends \Exception
{
    public function __construct(\Throwable $previous)
    {
        parent::__construct($previous->getMessage(), $previous->getCode(), $previous);
    }
}
