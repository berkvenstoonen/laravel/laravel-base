<?php

declare(strict_types=1);

namespace BeTo\Laravel\Exceptions;

interface MultipleExceptionsInterface extends ExtraDataInterface
{
    /**
     * @return array<\Exception>
     */
    public function getExtraExceptions(): array;
    public function getGroupedExceptionId(): string;
}
