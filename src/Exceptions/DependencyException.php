<?php
declare(strict_types=1);

namespace BeTo\Laravel\Exceptions;

use JetBrains\PhpStorm\Pure;

/**
 * @method \Throwable getPrevious()
 */
class DependencyException extends \Exception
{
    #[Pure] public function __construct(\Throwable $previous)
    {
        parent::__construct("A dependency has thrown an unknown error", 0, $previous);
    }
}
