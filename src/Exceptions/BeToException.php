<?php
declare(strict_types=1);

namespace BeTo\Laravel\Exceptions;

use JetBrains\PhpStorm\Pure;

abstract class BeToException extends \Exception
{
    #[Pure] public function __construct(string $message, ?\Throwable $previous = null, int $code = 0)
    {
        parent::__construct($message, $code, $previous);
    }
}
