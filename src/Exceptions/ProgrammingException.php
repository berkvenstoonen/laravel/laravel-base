<?php
declare(strict_types=1);

namespace BeTo\Laravel\Exceptions;

use JetBrains\PhpStorm\Pure;

final class ProgrammingException extends \Exception implements ExtraDataInterface
{
    #[Pure] public function __construct(
        string $message,
        ?\Throwable $previous = null,
        int $code = 0,
        private readonly array $extraData = [],
    )
    {
        parent::__construct($message, $code, $previous);
    }

    public function getExtraData(): array
    {
        return $this->extraData;
    }
}
