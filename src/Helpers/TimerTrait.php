<?php
declare(strict_types=1);

namespace BeTo\Laravel\Helpers;

use Illuminate\Console\OutputStyle;

trait TimerTrait
{
    protected OutputStyle $output;

    public function time(callable $callback): void
    {
        $start = microtime(true);
        $callback();
        $time = microtime(true) - $start;
        if ($time < 0.01) {
            $time = round($time * 1000000) . ' μs';
        } elseif ($time < 1) {
            $time = round($time * 1000) . ' ms';
        } elseif ($time < 10) {
            $time = round($time, 2) . ' s';
        } else {
            $time = round($time) . 's';
        }
        $this->output->writeln('Duration: <comment>' . $time . '</comment>');
    }
}
