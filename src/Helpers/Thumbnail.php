<?php

declare(strict_types=1);

namespace BeTo\Laravel\Helpers;

use BeTo\Laravel\Enums\ThumbnailSize;
use BeTo\Laravel\Exceptions\ProgrammingException;
use BeTo\Laravel\Models\Image\HasImageFromFileInterface;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Symfony\Component\HttpFoundation\Response;

final class Thumbnail
{
    public static function getThumbnailUrl(HasImageFromFileInterface $item, ThumbnailSize $size): string
    {
        if (!$item->hasImage()) {
            throw new ProgrammingException('Always make sure the model has an image before trying to get the thumbnail url');
        }
        return route('item.thumbnail', ['item' => $item, 'size' => $size->value, 'version' => $item->getVersion()]);
    }

    public static function getThumbnail(HasImageFromFileInterface $item, ThumbnailSize $size): Response
    {
        if (!$item->hasImage()) {
            throw new ProgrammingException('Always make sure the model has an image before trying to get the thumbnail url');
        }
        $path = $item->getThumbnailPath($size);
        if (!Storage::exists($path)) {
            self::create($item, $size);
        }
        return response()->file(Storage::path($path));
    }

    public static function clearAll(HasImageFromFileInterface $item): void
    {
        if (!$item->hasImage()) {
            abort(400, 'Always make sure the model has an image before trying to get the thumbnail url');
        }
        foreach (ThumbnailSize::cases() as $size) {
            $path = $item->getThumbnailPath($size);
            if (Storage::exists($path)) {
                Storage::delete($path);
            }
        }
    }

    public static function create(HasImageFromFileInterface $item, ThumbnailSize $size): void
    {
        Image::make(Storage::path($item->getImagePath()))->fit($size->value, $size->value)->save(Storage::path($item->getThumbnailPath($size)));
    }
}
