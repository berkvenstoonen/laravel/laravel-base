<?php

declare(strict_types=1);

namespace BeTo\Laravel\Helpers;

use App\Models\Team;
use App\Models\User;
use BeTo\Laravel\Rules\AllowedEmail;
use Illuminate\Validation\Rule;
use Webmozart\Assert\Assert;

final class EmailRules
{
    public static function getUnique(bool $required, Team|int $team = null, User|int $user = null, bool $includeTeams = true): array
    {
        $extraRules = [];
        if ($includeTeams) {
            $teamsUnique = Rule::unique('teams');
            if ($team !== null) {
                $teamsUnique->ignore($team);
            }
            $extraRules[] = $teamsUnique;
        }
        $usersUnique = Rule::unique('users');
        if ($user !== null) {
            $usersUnique->ignore($user);
        }
        $extraRules[] = $usersUnique;
        return self::getBase($required, ...$extraRules);
    }

    public static function getExists(string $table, bool $required = true): array
    {
        Assert::inArray($table, ['users', 'teams', 'team_invitations']);
        return self::getBase($required, Rule::exists($table));
    }

    public static function getUniqueWhere(string $table, \Closure $where, bool $required = true): array
    {
        Assert::inArray($table, ['users', 'teams', 'team_invitations']);
        return self::getBase($required, Rule::unique($table)->where($where));
    }

    private static function getBase(bool $required, ...$extraRules): array
    {
        return [
            $required ? 'required' : 'nullable',
            'string',
            config('app.env') === 'testing' ? 'email' : 'email:rfc,dns,filter_unicode',
            'max:255',
            new AllowedEmail(),
            ...$extraRules,
        ];
    }
}
