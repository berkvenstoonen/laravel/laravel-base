<?php
declare(strict_types=1);

namespace BeTo\Laravel\Helpers;

use Illuminate\Support\Facades\Route;

final readonly class Routes
{
    public static function get(
        string $name,
        string $url = null,
        string $view = null,
    ): \Illuminate\Routing\Route {
        return Route::get($url ?? $name, function () use ($view, $name) {
            return view($view ?? $name);
        })->name($name);
    }
}
