<?php
declare(strict_types=1);

namespace BeTo\Laravel\Helpers;

use BeTo\Laravel\Console\ExecuteShellCommand;
use Illuminate\Console\OutputStyle;
use Illuminate\Console\View\Components\Info;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Process\Exception\ProcessFailedException;

final class Build
{
    public static function build(): void
    {
        $output = new ConsoleOutput();
        $outputStyle = new OutputStyle(new StringInput(''), $output);
        (new Info($outputStyle))->render('Creating new Build.');
        $output->writeln('  Updating Composer (DEV)') ;
        ExecuteShellCommand::execute(['composer', 'update', '--no-interaction']);
        $output->writeln('  Updating NPM (DEV)');
        ExecuteShellCommand::execute(['npm', 'update', '--no-interaction']);
        ExecuteShellCommand::execute(['npm', 'install', '--no-interaction']);
        $output->writeln('  Trying to Customize');
        try {
            ExecuteShellCommand::execute(['php', 'artisan', 'customizations:apply']);
        } catch (ProcessFailedException) {
            $output->writeln('  <comment>customizations:apply</comment> not found');
        }
        $output->writeln('  Building');
        if (config('app.env') !== 'local') {
            ExecuteShellCommand::execute(['npm', 'run', 'production']);
            $output->writeln('  Updating Composer (PROD)');
            ExecuteShellCommand::execute(['composer', 'update', '--no-interaction', '--optimize-autoloader', '--no-dev']);
            $output->writeln('  Updating NPM (PROD)');
            ExecuteShellCommand::execute(['npm', 'update', '--no-interaction', '--omit=dev', '--omit=optional']);
        }
    }
}
