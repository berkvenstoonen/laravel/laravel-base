<?php
declare(strict_types=1);

namespace BeTo\Laravel\Helpers;

use Illuminate\View\ComponentAttributeBag;

final readonly class Attributes
{
    public static function withPrefix(ComponentAttributeBag $attributeBag, string $prefix): ComponentAttributeBag
    {
        $attributes = [];
        foreach ($attributeBag->getAttributes() as $attribute => $value) {
            if (self::startsWithPrefix($attribute, $prefix)) {
                $attributes[substr($attribute, strlen($prefix))] = $value;
            }
        }
        return new ComponentAttributeBag($attributes);
    }

    public static function withoutPrefix(ComponentAttributeBag $attributeBag, string ...$prefixes): ComponentAttributeBag
    {
        return $attributeBag->whereDoesntStartWith($prefixes);
    }

    private static function startsWithPrefix(string $key, string ...$prefixes): bool
    {
        foreach ($prefixes as $prefix) {
            if (str_starts_with($key, $prefix)) {
                return true;
            }
        }
        return false;
    }
}
