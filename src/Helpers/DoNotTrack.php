<?php
declare(strict_types=1);

namespace BeTo\Laravel\Helpers;

final class DoNotTrack
{
    public static function dnt(): bool
    {
        return filter_var(
            $_SERVER['DNT'] ?? $_SERVER['HTTP_DNT'] ?? false,
            FILTER_VALIDATE_BOOLEAN
        );
    }
}
