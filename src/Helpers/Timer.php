<?php
declare(strict_types=1);

namespace BeTo\Laravel\Helpers;

use Illuminate\Console\OutputStyle;

final class Timer
{
    use TimerTrait;

    private function __construct(
        protected OutputStyle $output,
    ) {
    }

    public static function create(OutputStyle $output): self
    {
        return new self($output);
    }
}
