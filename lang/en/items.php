<?php

return [
    'title'                  => 'Items',
    'add'                    => 'Create new Item',
    'all'                    => 'All Items',
    'multi-edit'             => 'Edit Multiple Items',
    'none-created'           => 'No items have been created yet',
    'none-with-value'        => 'No items have this value',
    'item-title'             => 'Item Title',
    'share-with'             => 'Share With',
    'shared-with'            => 'Shared With',
    'rotation'               => [
        'x' => 'X Rotation',
        'y' => 'Y Rotation',
        'z' => 'Z Rotation',
    ],
    'preview-unavailable'    => 'Still processing... Try again later',
    'extra-data-field'       => 'Optional File Collection',
    'extra-data-description' => 'If you have a zip with other variants of this 3D model (split parts, supported variants, etc) you can upload that here.',
    'none-found-title'       => 'No Items have been found.',
    'none-found-message'     => 'Try a different query or filters instead, or browse using the categories below.',
];
