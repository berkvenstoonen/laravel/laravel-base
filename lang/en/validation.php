<?php

return [
    'allowed_email' => 'This :attribute is not an allowed email address',
];
