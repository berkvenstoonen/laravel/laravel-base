<?php

return [
    'title'       => 'Teams',
    'settings'    => 'Team Settings',
    'details'     => 'Team Details',
    'description' => 'Create a new team to collaborate with others on projects.',
    'owner'       => 'Team Owner',
    'name'        => [
        'label'     => 'Team Name',
        'sub-label' => 'The team\'s name and owner information.',
    ],
    'email'       => [
        'label' => 'Email',
    ],
    'actions'     => [
        'manage'     => 'Manage Team',
        'create-new' => 'Create New Team',
        'create'     => 'Create New Team',
        'switch'     => 'Switch Teams',
    ],
    'delete'      => [
        'label'       => 'Delete Team',
        'sub-label'   => 'Permanently delete this team.',
        'description' => 'Once a team is deleted, all of its resources and data will be permanently deleted. Before deleting this team, please download any data or information regarding this team that you wish to retain.',
        'action'      => 'Delete Team',
        'modal'       => [
            'label'       => 'Delete Team',
            'description' => 'Are you sure you want to delete this team? Once a team is deleted, all of its resources and data will be permanently deleted.',
        ],
    ],
    'members'     => [
        'none'         => 'There are no members in this team',
        'label'        => 'Team Members',
        'sub-label'    => 'All of the people that are part of this team.',
        'actions'      => [
            'leave'  => 'Leave',
            'remove' => 'Remove',
        ],
        'add'          => [
            'label'       => 'Add Team Member',
            'sub-label'   => 'Add a new team member to your team, allowing them to collaborate with you.',
            'description' => 'Please provide the email address of the person you would like to add to this team.',
            'fields'      => [
                'role' => 'Role',
            ],
        ],
        'pending'      => [
            'label'     => 'Pending Team Invitations',
            'sub-label' => 'These people have been invited to your team and have been sent an invitation email. They may join the team by accepting the email invitation.',
        ],
        'manage-roles' => [
            'label' => 'Manage Role',
        ],
        'leave'        => [
            'label'         => 'Leave Team',
            'sub-label'     => 'Are you sure you would like to leave this team?',
            'can-not-leave' => 'This is the only team you have access to and therefore cannot leave it.',
        ],
        'remove'       => [
            'label'     => 'Remove Team Member',
            'sub-label' => 'Are you sure you would like to remove this person from the team?',
        ],
    ],
    'roles'       => [
        'admin'    => [
            'label'       => 'Manager',
            'description' => 'Managers can add and manage the members in the group. The managers can also remove items shared with the group.',
        ],
        'editor'   => [
            'label'       => 'Editor',
            'description' => 'Editors have the ability to share items with this group.',
        ],
        'observer' => [
            'label'       => 'Observer',
            'description' => 'Observers have the ability to view items shared with this group',
        ],
    ],
];
