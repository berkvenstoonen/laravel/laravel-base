<?php

return [
    'title'       => 'Groepen',
    'settings'    => 'Groep Instellingen',
    'details'     => 'Groep Details',
    'description' => 'Maak een nieuwe groep aan om items met andere te delen.',
    'owner'       => 'Groep Eigenaar',
    'name'        => [
        'label'     => 'Groep Naam',
        'sub-label' => 'De groepsnaam en eigenaar informatie.',
        'fields'    => [
            'name' => 'Groep Naam',
        ],
    ],
    'actions'     => [
        'manage'     => 'Beheer Groep',
        'create-new' => 'Maak een Nieuwe Groep',
        'create'     => 'Maak Groep',
        'switch'     => 'Verander van Groep',
    ],
    'delete'      => [
        'label'       => 'Verwijder Groep',
        'sub-label'   => 'Verwijder deze groep permanent.',
        'description' => 'Wanneer de groep verwijderd is worden alle items die alleen daarin gedeeld zijn verwijderd. Voordat je deze groep verwijderd, download alle items die je nog wilt bewaren.',
        'action'      => 'Verwijder Groep',
        'modal'       => [
            'label'       => 'Verwijder Groep',
            'description' => 'Weet je zeker dat je de groep wilt verwijderen? Wanneer de groep verwijderd is worden alle items die alleen daarin gedeeld zijn verwijderd.',
        ],
    ],
    'members'     => [
        'none'         => 'Er zitten geen gebruikers in dit team',
        'label'        => 'Groepsleden',
        'sub-label'    => 'Alle gebruikers die tot deze groep behoren.',
        'actions'      => [
            'leave'  => 'Verlaat',
            'remove' => 'Verwijder',
        ],
        'add'          => [
            'label'       => 'Voeg groepslid toe',
            'sub-label'   => 'Voeg nieuw groepslid toe aan de groep zodat je items met ze kan delen.',
            'description' => 'Voer het email adres in van de gebruiker die je toe wilt voegen aan de groep.',
            'fields'      => [
                'role' => 'Rol',
            ],
        ],
        'pending'      => [
            'label'     => 'Openstaande Groeps Uitnodigingen',
            'sub-label' => 'Deze gebruikers zijn via email uitgenodigd om lid te worden van de groep. Ze kunnen lid worden door op de uitnodiging in de email te klikken.',
        ],
        'manage-roles' => [
            'label' => 'Beheer Rollen',
        ],
        'leave'        => [
            'label'         => 'Verlaat Groep',
            'sub-label'     => 'Weet je zeker dat je deze groep wilt verlaten?',
            'can-not-leave' => 'Dit is de enige groep waar je toegang toe hebt, daarom kan je deze groep niet verlaten.',
        ],
        'remove'       => [
            'label'     => 'Verwijder Groepslid',
            'sub-label' => 'Weet je zeker dat je deze gebruiker uit de groep wilt zetten?',
        ],
    ],
    'roles'       => [
        'admin'    => [
            'label'       => 'Beheerder',
            'description' => 'Beheerders kunnen leden aan de groep toevoegen en aanpassen. Ook kunnen ze items die gedeeld zijn met de groep verwijderen.',
        ],
        'editor'   => [
            'label'       => 'Auteur',
            'description' => 'Auteurs kunnen items delen met de groep.',
        ],
        'observer' => [
            'label'       => 'Bekijker',
            'description' => 'Bekijkers kunnen items die met de groep gedeeld is bekijken.',
        ],
    ],
];
