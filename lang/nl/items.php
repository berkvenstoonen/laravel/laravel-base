<?php

return [
    'title'                  => 'Items',
    'add'                    => 'Item aanmaken',
    'all'                    => 'Alle Items',
    'multi-edit'             => 'Meerdere Items Aanpassen',
    'none-created'           => 'Er zijn nog geen items aangemaakt',
    'none-with-value'        => 'Er zijn geen items met deze waarde',
    'item-title'             => 'Item Titel',
    'share-with'             => 'Deel met',
    'shared-with'            => 'Gedeeld met',
    'rotation'               => [
        'x' => 'X Rotation',
        'y' => 'Y Rotation',
        'z' => 'Z Rotation',
    ],
    'preview-unavailable'    => 'Nog bezig met omzetten... Probeer later nog eens',
    'extra-data-field'       => 'Optionele bestanden collectie',
    'extra-data-description' => 'Als je een zip hebt met andere varianten van het 3D model (split parts, supported variants, etc) kan je deze hier uploaden.',
    'none-found-title'       => 'Er zijn geen items gebonden.',
    'none-found-message'     => 'Probeer andere zoektermen of filters, of bleder door de categorieën hieronder.',
];
