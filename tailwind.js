const defaultTheme = require("tailwindcss/defaultTheme");
module.exports = {
    mode    : 'jit',
    content : [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
        './vendor/laravel/jetstream/**/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
        './resources/js/**/*.js',
        './vendor/beto/**.js',
        './vendor/beto/**.blade.php',
        './node_modules/flowbite/**/*.js'
    ],
    darkMode: 'class',
    theme   : {
        extend: {
            animation : {
                'reverse-spin': 'reverse-spin 1s linear infinite'
            },
            keyframes : {
                'reverse-spin': {
                    from: {
                        transform: 'rotate(360deg)'
                    },
                }
            },
            fontFamily: {
                sans: ['Nunito', ...defaultTheme.fontFamily.sans],
            },
            screens   : {
                '3xl': '2000px',
            },
            width     : {
                '3xl': '2000px',
                '50p': '50%',
            },
            height    : {
                '50p': '50%',
            },
            maxWidth  : theme => {
                return {
                    'screen-3xl': theme('screens.3xl'),
                    '4xl'       : '75rem',
                    '6xl'       : '90rem',
                    '8xl'       : '120rem',
                };
            },
            colors    : {
                gray: {
                    950: '#0E131F',
                },
            },
        },
    },

    plugins: [
        require('@tailwindcss/forms'),
        require('@tailwindcss/typography'),
    ],
};
