import {defineConfig, loadEnv} from 'vite';
import laravel, {refreshPaths} from 'laravel-vite-plugin';
import {sentryVitePlugin} from '@sentry/vite-plugin';
import {execSync} from 'child_process';

module.exports = inputs => {
    if (inputs === undefined) {
        inputs = [
            'resources/css/app.scss',
            'resources/js/app.js',
        ];
    }
    return defineConfig(({ mode, command }) => {
        const env = loadEnv(mode, process.cwd(), ['SENTRY_']);
        const commitHash = execSync('git rev-parse --short HEAD').toString().trim();
        const options = {
            build: {
                sourcemap: true,
            },
            run: {
                sourcemap: true,
            },
            plugins: [
                laravel({
                    input: inputs,
                    refresh: [
                        ...refreshPaths,
                        'app/Livewire/**',
                    ],
                }),
                sentryVitePlugin({
                    org: 'beto',
                    project: 'finder',
                    include: './public/build/assets',
                    authToken: env.SENTRY_AUTH_TOKEN,
                    release: commitHash,
                }),
            ],
        };
        if (command === 'serve') {
            options.server = {
                https: {
                    hostname: 'localdev.finder.com',
                    key: '-----BEGIN RSA PRIVATE KEY-----\n' +
                        'MIIEpAIBAAKCAQEA1apR4uCkyHx+w0Nco1GubwA/AoCnACrraco+2ai0GhgoRgiQ\n' +
                        'ine6+R9C8BgxppK4hiK++DhBp5GXH5C1H4pp0GsZ+g7Cm+ifSRJgUNAhaF7v4YEo\n' +
                        'JBFTT4BKrITO6cFoDcu1O7olHPvI+TRj54Gzrkf0VtJZ2OlPjar7jOexF38jF1QN\n' +
                        '2/JYRyR5AL5d+FI5ANtrQ5Vd3unwKoouitX8NqEFhgUgVGeXliZpYBwkUEn6MSe6\n' +
                        'sWCHRRHcBLorvwFg3TmaAL9Qf1qZ50Le4LPFFRwSYv0Tp06yIUsCdx26hhR3CRZY\n' +
                        '0LMhP9mFjeLE35E529V7FVrGzLkOLbk6zmQpbQIDAQABAoIBAQCi5NLBEVsPrjpG\n' +
                        'S5jYunEuFxXypnqrWg1X/eSktEV6j2hE3Eq2Bkn5m6fAu/E+eSVMvjixrNIBGrII\n' +
                        'Up6DiVbamoyXY6Fik5q8btIXSlXt6FMhrQIZXUwSlFBZ7naBJ/5YZjGpc2TQD/VM\n' +
                        'aHCyEuQVvq29c2kkQdtwuiZFam5M5i6i+6czr1+EX+5WnVpm21eyVVp7SpkW7Nsb\n' +
                        'iVIxlrTkMPjDJRs0iBPPUDJfUqfBAhR/j3zKR2iP2IClTyplhWlVPODXJaOxf140\n' +
                        'nAClKHmi9BDCeAFudvpKIKd0D1WZ8QZqgtL2nl2jf6jTAgm3gTVtoHLzHd58MYgU\n' +
                        '3sfEr7zRAoGBAP/RpDTOGIm6ifz3J8EzG+J+ErJI0haDMmJM0RHOMM/AKJhTxZHz\n' +
                        'xaAmxYl79596t1y03lbm9bqWo/yvX05wX6TLmrvhwPsoADHy/1JAHIpd7tJCQbYh\n' +
                        'bVRpppFY+YzfFfNvtG8eJxQLwBRSqJsYe7lLrS+Ovd3wRlyZtiRxSnlvAoGBANXR\n' +
                        'Ch23BZZ01RXrZrDAbZQmiCOxQ1k4zfHL6TRfeK/2eJsqWSRexXYywUC1+7K0CK2B\n' +
                        'BWLrvZqyz0LOQSUjRA8ZminpGZ4p3A6zGYdVbkQ6YB61Wx9Zhw/0X6yCBLsZwFoE\n' +
                        'hc/oXAfygm9vROtSfq0vqJGZgUEE3RSd9TWJ90TjAoGAGxj7ydQi6UPnJfKQNXjS\n' +
                        'dVVhaZ0pE9Vl2t/ELZynsvnp0ujcvq8RufshZYTKIAyBq8JE4vOoB9G5IST6dXaV\n' +
                        'ywD+LPSm2nPMQX456V59J3L3vzHhgAdea1idmjvQg0mGK7kjZbnN7eOovv5aecNI\n' +
                        'J8KKX8VA+2deLKTjlEyj1OUCgYAaV3iRlJzBnn8qBihOmkZlvCGFlC8h7NeidGa0\n' +
                        'pxZtR0Ywh5UX3/0fxLz+Re1OZNO03HJXb4eaaXETX+mys5eM7LCLfUZEsA+GWBZL\n' +
                        'EAFXsoFpBxrEz/jNxGONEy+WBWQnMutxY8UCzaO4KtMo0q78kySHW4wLAJhkE9nD\n' +
                        '0M1/1QKBgQCt/Gv3c3L+54FReIUIlSsKOJ/7f5yiZ8d2f7JLOCOROtM/qoJrolQt\n' +
                        'TX42ntwH/YuRE4Un9okD0P/3WgfmdfKslDujhgvJlHH/YA45ECmqZjFealWMMFJw\n' +
                        'Og+c8xw2udSDzCLfTcTShT9PRY/DDIQ72nQU8qVCqh6jWi0T3rYMrQ==\n' +
                        '-----END RSA PRIVATE KEY-----',
                    cert: '-----BEGIN CERTIFICATE-----\n' +
                        'MIIEHTCCAwWgAwIBAgIUHrS+Su5Jt0r022LT1s6hg2tNpAQwDQYJKoZIhvcNAQEL\n' +
                        'BQAwgYwxCzAJBgNVBAYTAk5MMRYwFAYDVQQIDA1Ob29yZC1CcmFiYW50MRIwEAYD\n' +
                        'VQQHDAlFaW5kaG92ZW4xDTALBgNVBAoMBEJlVG8xETAPBgNVBAsMCG5sLmJlLXRv\n' +
                        'MREwDwYDVQQDDAhubC5iZS10bzEcMBoGCSqGSIb3DQEJARYNYmV0b0Bsb2NhbGRl\n' +
                        'djAeFw0yMjAzMDExOTAzNTZaFw0yNzAyMjgxOTAzNTZaMIGHMQswCQYDVQQGEwJO\n' +
                        'TDEWMBQGA1UECAwNTm9vcmQtQnJhYmFudDEPMA0GA1UEBwwGTGVlbmRlMQ0wCwYD\n' +
                        'VQQKDARCZVRvMQ0wCwYDVQQLDARCZVRvMREwDwYDVQQDDAhiZS10by5ubDEeMBwG\n' +
                        'CSqGSIb3DQEJARYPZmluZGVyQGxvY2FsZGV2MIIBIjANBgkqhkiG9w0BAQEFAAOC\n' +
                        'AQ8AMIIBCgKCAQEA1apR4uCkyHx+w0Nco1GubwA/AoCnACrraco+2ai0GhgoRgiQ\n' +
                        'ine6+R9C8BgxppK4hiK++DhBp5GXH5C1H4pp0GsZ+g7Cm+ifSRJgUNAhaF7v4YEo\n' +
                        'JBFTT4BKrITO6cFoDcu1O7olHPvI+TRj54Gzrkf0VtJZ2OlPjar7jOexF38jF1QN\n' +
                        '2/JYRyR5AL5d+FI5ANtrQ5Vd3unwKoouitX8NqEFhgUgVGeXliZpYBwkUEn6MSe6\n' +
                        'sWCHRRHcBLorvwFg3TmaAL9Qf1qZ50Le4LPFFRwSYv0Tp06yIUsCdx26hhR3CRZY\n' +
                        '0LMhP9mFjeLE35E529V7FVrGzLkOLbk6zmQpbQIDAQABo3oweDAfBgNVHSMEGDAW\n' +
                        'gBSDOvIOmZtiCFaPSl0tdCvfnFIfwDAJBgNVHRMEAjAAMAsGA1UdDwQEAwIE8DA9\n' +
                        'BgNVHREENjA0ghNsb2NhbGRldi5maW5kZXIuY29tgh1sb2NhbGRldi5maW5kZXIu\n' +
                        'Y29tLjEyNy4wLjAuMTANBgkqhkiG9w0BAQsFAAOCAQEAZSdacNBT3gTuJTPca73Y\n' +
                        'AaNptKZqLpgy+cWk8BUJ83s/CKBqzQiF0E0m2WDEiWcMqYfGEoNOXCI92ffcXtGC\n' +
                        'BBQwOGCU0KtHrJijqqfLiD8aSc9szw8ednvCP/U60I/SMCkWWD+oN/mXlYDdAxAe\n' +
                        '9gW91s5VowYbrpywXWiASDlGa9uAIVYBzRmnB+GG6Ixjs24r9XWk3fqJAxxMmM2k\n' +
                        'YYCN0MUlXgG2g2k7tn5ueALbF4xpiDiGMtquznW7M55I3waxrWRa43DOyDD7YgPW\n' +
                        'oizS1vUmPJ635L7tPus3QiaoM0nLwFXSpQtA4SYCFEUCh6z8t21xWzlFzQ9ZdBXK\n' +
                        'Wg==\n' +
                        '-----END CERTIFICATE-----'
                },
                origin: 'https://localdev.finder.com:5173',
                host: 'localdev.finder.com',
            };
        }
        return options;
    });
}
