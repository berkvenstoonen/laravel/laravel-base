<?php
declare(strict_types=1);

return [
    'throw_background_errors' => env('THROW_BACKGROUND_ERRORS', false),
    'sentry'                  => [
        'environment' => env('SENTRY_ENVIRONMENT'),
        'log_all'     => env('APP_DEBUG', false),
    ],
    'app_search'              => [
        'enabled' => env('APP_ENV') !== 'testing',
    ],
    'menu'                    => static function (): array {
        return [
            'home'               => route('home'),
            'primary'            => config('beto.primary-menu')(),
            'secondary'          => config('beto.secondary-menu')(),
            'secondary-features' => [
                'dark-mode-toggle' => true,
            ],
        ];
    },
    'primary-menu'            => static fn (): array => [],
    'secondary-menu'          => static fn (): array => [
        [
            'label'  => trans('beto::common.menu-items.policy'),
            'route'  => route('policy.show'),
            'active' => request()->routeIs('policy.show'),
        ],
    ],
    'include'                 => [
        'font-awesome' => false,
    ],
];
